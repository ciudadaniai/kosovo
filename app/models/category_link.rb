class CategoryLink < ApplicationRecord
  has_many :member_links

  has_one_attached :icon
  validates :icon, size: { less_than: 300.kilobytes },
            content_type: ['image/png','image/jpg','image/jpeg']

end
