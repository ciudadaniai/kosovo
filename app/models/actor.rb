class Actor < ApplicationRecord
  translates :name, :fallbacks_for_empty_translations => true
  accepts_nested_attributes_for :translations, allow_destroy: true

  has_and_belongs_to_many :initiatives
  has_and_belongs_to_many :stories

  scope :stories, -> (actor_id) { includes(:story).where('story.actor.id' => actor_id) }

  globalize_accessors
  validates :name, presence: true
  translation_class.validates :name, presence: true
  
  include PgSearch::Model
  multisearchable
  
  def self.rebuild_pg_search_documents
    connection.execute <<-SQL
     INSERT INTO pg_search_documents (searchable_type, searchable_id, content, created_at, updated_at)
       SELECT 'Actor' AS searchable_type,
              actors.id AS searchable_id,
              concat(actor_translations.name, ' ') AS content,
              now() AS created_at,
              now() AS updated_at
       FROM actors
       LEFT JOIN actor_translations
         ON actors.id = actor_translations.measurement_id
    SQL
  end

  pg_search_scope :search_by_name, against: :name, :associated_against => {
    translations: :name,
  }, using: { tsearch: { prefix: true }}

end
