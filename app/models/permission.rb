class Permission < ApplicationRecord
    has_paper_trail
    
    validates :subject_class, :action, :name, presence: true
end
