class Role < ApplicationRecord
    has_paper_trail

    validates :name, presence: true

    has_many :users
    has_and_belongs_to_many :permissions
end
