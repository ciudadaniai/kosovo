class StoryCategory < ApplicationRecord
    translates :title, :description, :fallbacks_for_empty_translations => true
    accepts_nested_attributes_for :translations, allow_destroy: true

    globalize_accessors
    translation_class.validates :title, presence: true

    has_many :stories
end
