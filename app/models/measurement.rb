class Measurement < ApplicationRecord
  include Filterable
  translates :title, :summary, :description, :fallbacks_for_empty_translations => true
  accepts_nested_attributes_for :translations, allow_destroy: true

  translation_class.validates :title, presence: true

  has_many :initiatives
  belongs_to :measurement_category

  acts_as_taggable
  acts_as_taggable_on :tags

  scope :category, -> (measurement_category_id) { where measurement_category_id: measurement_category_id }

  include PgSearch::Model
  multisearchable

  def self.rebuild_pg_search_documents
    connection.execute <<-SQL
     INSERT INTO pg_search_documents (searchable_type, searchable_id, content, created_at, updated_at)
     SELECT 'Measurement' AS searchable_type,
             m1.id AS searchable_id,
             concat(
             -- Merge all titles of this measurement
             array_to_string(array(
                 SELECT title
                     FROM measurement_translations
                     WHERE m1.id = measurement_translations.measurement_id
                 ),' '),
             ' ',
             -- Merge all summaries
             array_to_string(array(
                 SELECT summary
                     FROM measurement_translations
                     WHERE m1.id = measurement_translations.measurement_id
                 ),' '),
             ' ',
             -- Merge all descriptions
             array_to_string(array(
                 SELECT description
                     FROM measurement_translations
                     WHERE m1.id = measurement_translations.measurement_id
                 ),' '),
             ' ',
             -- Merge all tags
             array_to_string(array(
                 SELECT name
                     FROM tags
                     INNER JOIN taggings
                         ON taggings.tag_id = tags.id
                         AND taggings.taggable_type = 'Measurement'
                     INNER JOIN measurements
                         ON measurements.id = taggings.taggable_id
                     WHERE measurements.id = m1.id
                 ), ' ')) AS content,
             now() AS created_at,
             now() AS updated_at
     FROM measurements m1
    SQL
  end

  def next
    Measurement.where(id: id+1).exists? ? Measurement.find(id+1) : Measurement.first
  end

  def previous
    Measurement.where(id: id-1).exists? ? Measurement.find(id-1) : Measurement.last
  end

  pg_search_scope :search_by_title, against: :title, :associated_against => {
    translations: :title,
  }, using: { tsearch: { prefix: true }}

end
