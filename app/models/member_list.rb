class MemberList < ApplicationRecord
    translates :name, :fallbacks_for_empty_translations => true
    accepts_nested_attributes_for :translations, allow_destroy: true

    globalize_accessors
    translation_class.validates :name, presence: true
    has_many :members
end
