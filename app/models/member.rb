class Member < ApplicationRecord
    translates :bio, :fallbacks_for_empty_translations => true
    accepts_nested_attributes_for :translations, allow_destroy: true

    validates :name, presence: true

    has_one_attached :picture
    belongs_to :member_list
    has_and_belongs_to_many :stories
    has_many :member_links

    validates :picture, size: { less_than: 100.kilobytes },
                        content_type: ['image/png','image/jpg','image/jpeg']
end
