class Initiative < ApplicationRecord
  include Filterable
  translates :title, :summary, :description, :fallbacks_for_empty_translations => true
  accepts_nested_attributes_for :translations, allow_destroy: true

  validates :title, presence: true
  translation_class.validates :title, presence: true

  belongs_to :measurement
  has_one :measurement_category, through: :measurement
  belongs_to :initiative_status
  has_and_belongs_to_many :actors
  has_and_belongs_to_many :areas

  acts_as_taggable
  acts_as_taggable_on :tags

  scope :category, -> (category_id) { includes(:measurement).where('measurements.measurement_category.id' => category_id) }
  scope :initiative_status, -> (status_id) { where initiative_status_id: status_id }
  scope :actor, ->(actor_id) { joins(:actors).where(actors_initiatives: {actor_id: actor_id} ) }
  scope :area, ->(area_id) { joins(:areas).where(areas_initiatives: {area_id: area_id} ) }

  include PgSearch::Model
  multisearchable

  def self.rebuild_pg_search_documents
    connection.execute <<-SQL
     INSERT INTO pg_search_documents (searchable_type, searchable_id, content, created_at, updated_at)
        SELECT 'Initiative' AS searchable_type,
          i1.id AS searchable_id,
          concat(
              -- Merge all titles of this story
              array_to_string(array(
                  SELECT title
                      FROM initiative_translations
                      WHERE i1.id = initiative_translations.initiative_id
              ),' '),
              ' ',
              -- Merge all summaries
              array_to_string(array(
                  SELECT summary
                      FROM initiative_translations
                      WHERE i1.id = initiative_translations.initiative_id
              ),' '),
              ' ',
              -- Merge all descriptions
              array_to_string(array(
                  SELECT description
                      FROM initiative_translations
                      WHERE i1.id = initiative_translations.initiative_id
              ),' '),
              ' ',
              -- Merge all tags
              array_to_string(array(
                  SELECT name
                      FROM tags
                      INNER JOIN taggings
                          ON taggings.tag_id = tags.id
                          AND taggings.taggable_type = 'Initiative'
                      INNER JOIN initiatives
                          ON initiatives.id = taggings.taggable_id
                      WHERE initiatives.id = i1.id
                  ), ' ')) AS content,
          now() AS created_at,
          now() AS updated_at
        FROM initiatives i1
    SQL
  end

  def status_name
    initiative_status.status
  end

  def param_status_name
    initiative_status.status.parameterize
  end

  def get_color
    if initiative_status.color.nil?
      return "343a40"
    else
      return initiative_status.color
    end
  end

  def name_actors
    actors.map{|a| a.name}.join(', ')
  end

  def name_areas
    areas.map{|a| a.name}.join(', ')
  end

  def get_areas
    areas
  end

  def get_all_measurements
     Measurement.all.map{|x| x.title}
  end

  pg_search_scope :search_by_title, against: :title, :associated_against => {
    translations: :title,
  }, using: { tsearch: { prefix: true }}
  
end
