class CarouselSlot < ApplicationRecord
    translates :link_text, :carousel_text, :fallbacks_for_empty_translations => true
    accepts_nested_attributes_for :translations, allow_destroy: true

    has_paper_trail

    has_one_attached :picture

    globalize_accessors
    translation_class.validates :link_text, presence: true
    translation_class.validates :carousel_text, presence: true

    validates :link, presence: true

    validates :picture, attached: true,
                    content_type: ['image/png', 'image/jpg', 'image/jpeg'],
                    dimension: {
                        width: { min: 2109, max: 8436 },
                        height: { min: 850, max: 3400 }
                    }

    before_save do
        if self.picture.nil?
            self.picture.attach(io: File.open(File.join(Rails.root, "/app/assets/images/image.png")), filename: "image")
        end
    end
end
  