class MemberLink < ApplicationRecord
  belongs_to :member
  belongs_to :category_link

  # Esto permite ver la url en la vista de admin, en vez del id
  def name
    link
  end

end
