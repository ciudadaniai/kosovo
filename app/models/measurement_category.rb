class MeasurementCategory < ApplicationRecord
  translates :name, :description
  has_many :measurements
  has_many :initiatives, through: :measurements

  has_one_attached :picture
  has_one_attached :graphic

  globalize_accessors
  translation_class.validates :name, presence: true
  validates :name, presence: true

  validates :picture, size: { less_than: 300.kilobytes },
                      content_type: ['image/png','image/jpg','image/jpeg']

  validates :graphic, size: { less_than: 500.kilobytes },
    content_type: ['image/png','image/jpg','image/jpeg']
end
