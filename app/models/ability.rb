class Ability
  include CanCan::Ability

  def initialize(user)
    user.role.permissions.each do |permission|
      # p permission.inspect
      if !permission.modifier.nil?
        p "*******************************************************************Setting up permission with modifier"
        str = "can :" + permission.action + ", " + permission.subject_class + ", " + permission.modifier
        p str
        eval(str)
      else
        if permission.subject_class == "all"
          can permission.action.to_sym, permission.subject_class.to_sym
        elsif permission.subject_class == "rails_admin" || permission.subject_class == "dashboard"
          can permission.action.to_sym, permission.subject_class.to_sym
        else
          can permission.action.to_sym, permission.subject_class.constantize
        end
      end
    end
    # can :rails_admin, :export, Story
    # can :manage, :tag
    # can :manage, :user
    # can :read, Story, user: { id: user.id } 
    # can :create, Story
    # can :view, Story, user: { id: user.id } 
    # modifier = "user_id: user.id"
    # # can :edit, Story, modifier  
    # str = "can :read, Story, #{modifier}"
    # eval(str)
    
    # modifier = "user_id: user.id"
    # # can :edit, Story, modifier  
    # str = "can :edit, Story, #{modifier}"
    # p " ************************_: " + str.to_s
    # eval(str)
    # can :manage, Story, user: { id: user.id } 
    # can :index, Story
  end
end

# def initialize(user)
    # Define abilities for the passed in user here. For example:
    #
    #   user ||= User.new # guest user (not logged in)
    #   if user.admin?
    #     can :manage, :all
    #   else
    #     can :read, :all
    #   end
    #
    # The first argument to `can` is the action you are giving the user
    # permission to do.
    # If you pass :manage it will apply to every action. Other common actions
    # here are :read, :create, :update and :destroy.
    #
    # The second argument is the resource the user can perform the action on.
    # If you pass :all it will apply to every resource. Otherwise pass a Ruby
    # class of the resource.
    #
    # The third argument is an optional hash of conditions to further filter the
    # objects.
    # For example, here the user can only update published articles.
    #
    #   can :update, Article, :published => true
    #
    # See the wiki for details:
    # https://github.com/CanCanCommunity/cancancan/wiki/Defining-Abilities
#  end

