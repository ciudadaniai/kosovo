class Story < ApplicationRecord
  translates :title, :summary, :content, :fallbacks_for_empty_translations => true
  accepts_nested_attributes_for :translations, allow_destroy: true

  translation_class.validates :title, presence: true

  has_paper_trail
  belongs_to :user
  belongs_to :story_category
  has_and_belongs_to_many :actors
  has_and_belongs_to_many :members

  acts_as_taggable
  acts_as_taggable_on :tags

  has_one_attached :picture

  has_many_attached :gallery

  paginates_per 15

  include PgSearch::Model
  multisearchable

  def self.rebuild_pg_search_documents
    connection.execute <<-SQL
     INSERT INTO pg_search_documents (searchable_type, searchable_id, content, created_at, updated_at)
      SELECT 'Story' AS searchable_type,
          s1.id AS searchable_id,
          concat(
              -- Merge all titles of this story
              array_to_string(array(
                  SELECT title
                      FROM story_translations
                      WHERE s1.id = story_translations.story_id                
                  ),' '),
              ' ',
              -- Merge all summaries
              array_to_string(array(
                  SELECT summary
                      FROM story_translations
                      WHERE s1.id = story_translations.story_id                
                  ),' '),
              ' ',
              -- Merge all content
              array_to_string(array(
                  SELECT content
                      FROM story_translations
                      WHERE s1.id = story_translations.story_id
                  ),' '),
              ' ',
              -- Merge all tags
              array_to_string(array(
                  SELECT name
                      FROM tags
                      INNER JOIN taggings
                          ON taggings.tag_id = tags.id
                          AND taggings.taggable_type = 'Story'
                      INNER JOIN stories
                          ON stories.id = taggings.taggable_id
                      WHERE stories.id = s1.id
              ), ' ')) AS content,
              now() AS created_at,
              now() AS updated_at
      FROM stories s1
    SQL
  end

  pg_search_scope :search_by_title, against: :title, :associated_against => {
    translations: :title,
  }, using: { tsearch: { prefix: true }}

end
