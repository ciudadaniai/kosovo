class Area < ApplicationRecord
  translates :name, :fallbacks_for_empty_translations => true
  accepts_nested_attributes_for :translations, allow_destroy: true

  has_and_belongs_to_many :initiatives
  validates :name, presence: true
  translation_class.validates :name, presence: true

  validates :latitude, presence: true
  validates :longitude, presence: true

  include PgSearch::Model

  pg_search_scope :search_by_name, against: :name, :associated_against => {
    translations: :name,
  }, using: { tsearch: { prefix: true }}

end
