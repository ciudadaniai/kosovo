class InitiativeStatus < ApplicationRecord
    translates :status, :description, :fallbacks_for_empty_translations => true
    accepts_nested_attributes_for :translations, allow_destroy: true

    validates :status, presence: true
    translation_class.validates :status, presence: true
    globalize_accessors

    has_many :initiatives

    validates_format_of :color, with: /\A#?(?:[A-F0-9]{3}){1,2}\z/i

    def name
      status.nil? ? id : status
    end
end
