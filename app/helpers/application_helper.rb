module ApplicationHelper
    def markdown(text)
        markdown = Redcarpet::Markdown.new(Redcarpet::Render::HTML, autolink: true, tables: true)
        markdown.render(text).html_safe
    end

    def make_any_url(text)

        # If we're directing to a URL within the page, then we must not change it
        # and return the localized URL
        if text.include? "kosovotrustbuilding.com"
            is_localized = text.include?('kosovotrustbuilding.com/en/') or text.include?('kosovotrustbuilding.com/sq/') or text.include?('kosovotrustbuilding.com/sr/')
            if is_localized
                text = text.sub 'kosovotrustbuilding.com/en/', "kosovotrustbuilding.com/#{I18n.locale}/"
                text = text.sub 'kosovotrustbuilding.com/sq/', "kosovotrustbuilding.com/#{I18n.locale}/"
                text = text.sub 'kosovotrustbuilding.com/sr/', "kosovotrustbuilding.com/#{I18n.locale}/"
            else
                text = text.sub 'kosovotrustbuilding.com/', "kosovotrustbuilding.com/#{I18n.locale}/"
            end
        end
        make_external_url text
    end

    def make_external_url(text)
        if text.start_with?('mailto:', 'http:', 'https:')
            text
        else
            'http://' + text
        end
    end

    def get_resized_image_url(img)
        url_for(
            img.variant(
                combine_options: {
                    crop: "2109x850+0+0",
                    gravity: "center"}
                    ).processed)
    end
end
