class ApplicationMailer < ActionMailer::Base

  before_action { @recipient_email = @params[:params][:email] }

  default to:       -> { 'office@newsocialinitiative.org' },
          from:     -> { @recipient_email }
          # ,
          # reply_to: -> { @recipient_email }

  layout 'mailer'

  def contact_email
    subject = "[#{@params[:params][:category]}][#{@params[:params][:organization]}] Contact from Kosovo Trust Building"

    mail(subject: subject, params: @params)
  end
end
