class HomeController < ApplicationController

  def index
    @measurements_categories = MeasurementCategory.all
    @stories = Story.order('created_at DESC').limit(2)
    @highlighted = Story.where(highlight: true).order('created_at DESC').limit(3)
    @recommendations_count = Measurement.all.count
    @initiatives_count = Initiative.all.uniq { |i| i[:title] }.count
    @actors_count = Actor.all.count
    @champions_count = StoryCategory.where(id: 4)&.last&.stories&.count
  end

  def contact
    p contact_params
    if verify_recaptcha
      ApplicationMailer.with(params: contact_params).contact_email.deliver_later
      respond_to do |format|
        format.js { render :partial => 'shared/success.js.erb' }
      end
    else
      respond_to do |format|
        format.js { render :partial => 'shared/fail.js.erb' }
      end
    end
  end

  def start
  end

  def recommendations
    @measurements_categories = MeasurementCategory.all
  end

  private

    def contact_params
      params.permit(:category, :name, :email, :message, hashtags:[])
    end

end
