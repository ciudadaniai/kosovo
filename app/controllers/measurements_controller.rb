class MeasurementsController < ApplicationController
  before_action :set_paper_trail_whodunnit
  #devise so that only logged-in user can access
  before_action :authenticate_user!, except:[:show, :index, :search, :search_all, :by_location]

  #only user with super admin role can access
  # before_action :is_admin?
  before_action :load_and_authorize_resource, except:[:show, :index, :search, :search_all, :by_location]
  before_action :load_permissions , except:[:show, :index, :search, :search_all, :by_location]# call this after load_and_authorize else it gives a cancan error
  before_action :set_measurement, only: [:show, :edit, :update, :destroy]

  def self.permission
    return "Measurement"
  end

  # GET /stories
  # GET /stories.json
  def index
    @initiative_status = InitiativeStatus.all
    if params[:sdg_id].present?
      @measurements = Measurement.where.not(sdg: nil).map { |m| m if m.sdg.split(",").include?(params[:sdg_id].to_s) }.compact
      return
    end
    if params[:category_id].present?
      @measurement_category = MeasurementCategory.find(params[:category_id])
      cookies[:last_seen_category] = @measurement_category.id
      @measurements = Measurement.where(:measurement_category_id => params[:category_id]).order(id: :asc)
    else
      # TODO: Change to translation
      redirect_to measurements_categories_path, status: :found, notice:  ""
    end
  end

  def by_location
    @area = Area.find(params[:area_id])
    @initiatives = @area.initiatives
  end

  # GET /stories/1
  # GET /stories/1.json
  def show
    @initiative_status = InitiativeStatus.all
  end

  # GET /stories/new
  def new
    @measurement = Measurement.new
  end

  # GET /stories/1/edit
  def edit
  end

  # POST /stories
  # POST /stories.json
  def create
    @measurement = Measurement.new(measurement_params)

    respond_to do |format|
      if @measurement.save
        format.html { redirect_to @measurement, notice: 'Measurement was successfully created.' }
        format.json { render :show, status: :created, location: @measurement }
      else
        format.html { render :new }
        format.json { render json: @measurement.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /stories/1
  # PATCH/PUT /stories/1.json
  def update
    respond_to do |format|
      if @measurement.update(measurement_params)
        format.html { redirect_to @measurement, notice: 'Measurement was successfully updated.' }
        format.json { render :show, status: :ok, location: @measurement }
      else
        format.html { render :edit }
        format.json { render json: @measurement.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /stories/1
  # DELETE /stories/1.json
  def destroy
    @measurement.destroy
    respond_to do |format|
      format.html { redirect_to measurements_url, notice: 'Measurement was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  # POST /search
  # POST /search.json
  def search
    respond_to do |format|
      format.html do
        @household_items = @moving.household_items
        render :index
      end
      format.js do
        if params[:filter].present?
          @measurements = Measurement.filter(query_params.slice(:category))
        else
          @measurements = Measurement.filter(query_params.slice(:category))
          @initiatives = Initiative.filter(query_params.slice(:category, :initiative_status, :actor, :area))
        end
      end
    end
  end

  def search_all
    respond_to do |format|
      format.html do
        value = search_params.slice(:search)
        @results = PgSearch.multisearch(value["search"])
        p "Results: #{@results}"
        @measurements = []
        @stories = []
        @initiatives = []
        @results.each do |r|
          p "Showing result... #{r}"
          if r.searchable_type == 'Measurement'
            @measurements.push(Measurement.find(r.searchable_id))
          elsif r.searchable_type == 'Initiative'
            @initiatives.push(Initiative.find(r.searchable_id))
            @initiatives.uniq! { |a| a['title'] }
          elsif r.searchable_type == 'Story'
            @stories.push(Story.find(r.searchable_id))
          end
        end
        @search_params = value["search"]
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_measurement
      @measurement = Measurement.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def measurement_params
      params.require(:measurement).permit(:title, :summary, :description, :tag_list, :sdg)
    end

    def query_params
      params.require(:query).permit(:category, :initiative_status, :actor, :area)
    end

    def search_params
      # p "params"
      # p params
      params.permit(:search)
    end

end
