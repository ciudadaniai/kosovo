class EventsController < ApplicationController
  before_action :set_locale

  def events_json
    @events = Event.all
    monthly = []
    @events.each do |event|
      monthly << {
        id: event.id,
        name: event.name,
        startdate: event.startdate.to_date,
        enddate: event.enddate.to_date,
        starttime: event.starttime.strftime("%H:%M"),
        endtime: event.endtime.strftime("%H:%M"),
        color: "##{event.color}",
        url: event.url
      }
    end
    @events_json =  {
      monthly: monthly
    }
    render :json => @events_json
  end
end