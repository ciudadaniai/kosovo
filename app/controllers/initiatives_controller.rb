class InitiativesController < ApplicationController
  before_action :set_locale

  def index
    @initiatives = Initiative.where(measurement_id: params[:measurement_id]).order(start_date: :desc)
    @initiatives = Initiative.tagged_with(params[:theme_id]) if params[:for_theme] == "true"
    @initiatives = MeasurementCategory.where(id: params[:category_id]).last.initiatives if params[:for_recommendation] == "true"
    render json: @initiatives.as_json(only: [:id, :title, :start_date, :end_date], methods: [:status_name, :param_status_name, :get_color, :name_actors, :get_areas])
  end
  
  private
    # Use callbacks to share common setup or constraints between actions.
    def set_initiative
      @initiative = Initiative.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def initiative_params
      params.require(:initiative).permit(:title, :summary, :description, :link, :tag_list)
    end
end
