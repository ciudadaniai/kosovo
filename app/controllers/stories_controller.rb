class StoriesController < ApplicationController
  before_action :set_paper_trail_whodunnit
  #devise so that only logged-in user can access
  before_action :authenticate_user!, except:[:show, :index]

  #only user with super admin role can access
  # before_action :is_admin?
  before_action :load_and_authorize_resource, except:[:show, :index]
  before_action :load_permissions , except:[:show, :index]# call this after load_and_authorize else it gives a cancan error
  before_action :set_story, only: [:show, :edit, :update, :destroy]


  def self.permission
    return "Story"
  end

  # GET /stories
  # GET /stories.json
  def index
    @stories = Story.where(story_category_id: params[:category_id]).order('created_at DESC').page params[:page] if params[:category_id]
    @stories ||= Story.all.order('created_at DESC').page params[:page]
    @categories = StoryCategory.where.not(id: [5,6])
  end

  # GET /stories/1
  # GET /stories/1.json
  def show
    @story = Story.find(params[:id])
    @members = @story.members
    @latest_stories = Story.where.not(id: params[:id]).order('created_at DESC').limit(2)
  end

  # GET /stories/new
  def new
    @story = Story.new
  end

  # GET /stories/1/edit
  def edit
  end

  # POST /stories
  # POST /stories.json
  def create
    @story = Story.new(story_params)

    respond_to do |format|
      if @story.save
        format.html { redirect_to @story, notice: 'Story was successfully created.' }
        format.json { render :show, status: :created, location: @story }
      else
        format.html { render :new }
        format.json { render json: @story.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /stories/1
  # PATCH/PUT /stories/1.json
  def update
    respond_to do |format|
      if @story.update(story_params)
        format.html { redirect_to @story, notice: 'Story was successfully updated.' }
        format.json { render :show, status: :ok, location: @story }
      else
        format.html { render :edit }
        format.json { render json: @story.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /stories/1
  # DELETE /stories/1.json
  def destroy
    @story.destroy
    respond_to do |format|
      format.html { redirect_to stories_url, notice: 'Story was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_story
      @story = Story.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def story_params
      params.require(:story).permit(:title, :summary, :content, :user_id, :tag_list)
    end
end
