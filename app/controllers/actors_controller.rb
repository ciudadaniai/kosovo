class ActorsController < ApplicationController
  before_action :set_locale

  def index
  end

  def show
    @actor = Actor.find(params[:id])
    @stories = @actor.stories
    @initiatives = @actor.initiatives
    @categories = StoryCategory.all
  end

  def new
  end

  def edit
  end

  def destroy
  end

end
