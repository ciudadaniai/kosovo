class InitiativeStatusesController < ApplicationController
  before_action :set_locale

  def index
  end

  def show
  end

  def new
  end

  def edit
  end

  def destroy
  end

end
