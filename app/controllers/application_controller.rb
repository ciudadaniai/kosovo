class ApplicationController < ActionController::Base
  protect_from_forgery
  before_action :set_locale
  rescue_from CanCan::AccessDenied do |exception|
    flash[:alert] = "Access denied. You are not authorized to access the requested page."
    redirect_to root_path and return
  end

  protected

  #derive the model name from the controller. egs UsersController will return User
  def self.permission
    name = self.name.gsub('Controller','').singularize.split('::').last.constantize.name rescue nil
    return name
  end

  def current_ability
    @current_ability ||= Ability.new(current_user)
  end

  #load the permissions for the current user so that UI can be manipulated
  def load_permissions
    default_permission
    @current_permissions = current_user.role.permissions.collect{|i| [i.subject_class, i.action]} || p
  end

  def default_url_options
    { locale: I18n.locale }
  end

  private
    def set_locale
      I18n.locale = extract_locale || I18n.default_locale
    end

    def extract_locale
      parsed_locale = params[:locale] || request.env['HTTP_ACCEPT_LANGUAGE'].scan(/^[a-z]{2}/)[0] if request.env['HTTP_ACCEPT_LANGUAGE']
      I18n.available_locales.map(&:to_s).include?(parsed_locale) ? parsed_locale : nil
    end

end
