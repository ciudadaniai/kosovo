class MeasurementsCategoriesController < ApplicationController
  before_action :set_paper_trail_whodunnit
  #devise so that only logged-in user can access
  before_action :authenticate_user!, except:[:show, :index]

  #only user with super admin role can access
  # before_action :is_admin?
  before_action :load_and_authorize_resource, except:[:show, :index]
  before_action :load_permissions , except:[:show, :index]# call this after load_and_authorize else it gives a cancan error
  before_action :set_measurement_category, only: [:show, :edit, :update, :destroy]

  def self.permission
    return "MeasurementCategory"
  end

  # GET /stories
  # GET /stories.json
  def index
    @measurements_categories = MeasurementCategory.all
    @measurements_categories.each do |mc|
      p mc
    end
    @initiative_status = InitiativeStatus.all
  end

  # GET /stories/1
  # GET /stories/1.json
  def show
  end

  # GET /stories/new
  def new
    @measurement_category = MeasurementCategory.new
  end

  # GET /stories/1/edit
  def edit
  end

  # POST /stories
  # POST /stories.json
  def create
    @measurement_category = MeasurementCategory.new(measurement_category_params)

    respond_to do |format|
      if @measurement_category.save
        format.html { redirect_to @measurement_category, notice: 'Measurement Category was successfully created.' }
        format.json { render :show, status: :created, location: @measurement_category }
      else
        format.html { render :new }
        format.json { render json: @measurement_category.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /stories/1
  # PATCH/PUT /stories/1.json
  def update
    respond_to do |format|
      if @measurement_category.update(measurement_category_params)
        format.html { redirect_to @measurement_category, notice: 'Measurement Category was successfully updated.' }
        format.json { render :show, status: :ok, location: @measurement_category }
      else
        format.html { render :edit }
        format.json { render json: @measurement_category.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /stories/1
  # DELETE /stories/1.json
  def destroy
    @measurement_category.destroy
    respond_to do |format|
      format.html { redirect_to stories_url, notice: 'Measurement Category was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_measurement_category
      @measurement_category = MeasurementCategory.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def measurement_category_params
      params.require(:measurement_category).permit(:name, :description)
    end
end
