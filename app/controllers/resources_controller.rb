class ResourcesController < ApplicationController
  def index
    @categories = StoryCategory.where(id: [5,6])
    @stories = Story.where(story_category_id: params[:category_id]).order('created_at DESC').page params[:page] if params[:category_id]
    @stories ||= Story.where(story_category_id: 5).order('created_at DESC').page params[:page]
  end
end
