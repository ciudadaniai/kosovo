class Covid19Controller < ApplicationController
  def index
    @stories = Story.tagged_with("COVID19").order('created_at DESC').page params[:page]
  end
end
