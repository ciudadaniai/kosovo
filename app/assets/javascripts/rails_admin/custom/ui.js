//= require_tree .
//= require bootstrap-wysihtml5/wysihtml5x-toolbar.js
//= require bootstrap-wysihtml5/handlebars.runtime.min.js

//= require bootstrap-wysihtml5/minimum

jQuery(document).ready(function($) {

  $('#story_translations_attributes_0_content').each(function(i, elem) {
    $(elem).wysihtml5({
      toolbar: {
        "font-styles": true, // Font styling, e.g. h1, h2, etc.
        "emphasis": true, // Italics, bold, etc.
        "lists": true, // (Un)ordered lists, e.g. Bullets, Numbers.
        "html": true, // Button which allows you to edit the generated HTML.
        "link": true, // Button to insert a link.
        "image": true, // Button to insert an image.
        "color": false, // Button to change color of font
        "blockquote": true, // Blockquote
      }
    });
  });

  $('#story_translations_attributes_1_content').each(function(i, elem) {
    $(elem).wysihtml5({
      toolbar: {
        "font-styles": true, // Font styling, e.g. h1, h2, etc.
        "emphasis": true, // Italics, bold, etc.
        "lists": true, // (Un)ordered lists, e.g. Bullets, Numbers.
        "html": true, // Button which allows you to edit the generated HTML.
        "link": true, // Button to insert a link.
        "image": true, // Button to insert an image.
        "color": false, // Button to change color of font
        "blockquote": true, // Blockquote
      }
    });
  });

  $('#story_translations_attributes_2_content').each(function(i, elem) {
    $(elem).wysihtml5({
      toolbar: {
        "font-styles": true, // Font styling, e.g. h1, h2, etc.
        "emphasis": true, // Italics, bold, etc.
        "lists": true, // (Un)ordered lists, e.g. Bullets, Numbers.
        "html": true, // Button which allows you to edit the generated HTML.
        "link": true, // Button to insert a link.
        "image": true, // Button to insert an image.
        "color": false, // Button to change color of font
        "blockquote": true, // Blockquote
      }
    });
  });

  $('#initiative_translations_attributes_0_description').each(function(i, elem) {
    $(elem).wysihtml5({
      toolbar: {
        "font-styles": true, // Font styling, e.g. h1, h2, etc.
        "emphasis": true, // Italics, bold, etc.
        "lists": true, // (Un)ordered lists, e.g. Bullets, Numbers.
        "html": true, // Button which allows you to edit the generated HTML.
        "link": true, // Button to insert a link.
        "image": true, // Button to insert an image.
        "color": false, // Button to change color of font
        "blockquote": true, // Blockquote
      }
    });
  });

  $('#initiative_translations_attributes_1_description').each(function(i, elem) {
    $(elem).wysihtml5({
      toolbar: {
        "font-styles": true, // Font styling, e.g. h1, h2, etc.
        "emphasis": true, // Italics, bold, etc.
        "lists": true, // (Un)ordered lists, e.g. Bullets, Numbers.
        "html": true, // Button which allows you to edit the generated HTML.
        "link": true, // Button to insert a link.
        "image": true, // Button to insert an image.
        "color": false, // Button to change color of font
        "blockquote": true, // Blockquote
      }
    });
  });

  $('#initiative_translations_attributes_2_description').each(function(i, elem) {
    $(elem).wysihtml5({
      toolbar: {
        "font-styles": true, // Font styling, e.g. h1, h2, etc.
        "emphasis": true, // Italics, bold, etc.
        "lists": true, // (Un)ordered lists, e.g. Bullets, Numbers.
        "html": true, // Button which allows you to edit the generated HTML.
        "link": true, // Button to insert a link.
        "image": true, // Button to insert an image.
        "color": false, // Button to change color of font
        "blockquote": true, // Blockquote
      }
    });
  });

  $('#member_translations_attributes_0_bio').each(function(i, elem) {
    $(elem).wysihtml5({
      toolbar: {
        "font-styles": true, // Font styling, e.g. h1, h2, etc.
        "emphasis": true, // Italics, bold, etc.
        "lists": true, // (Un)ordered lists, e.g. Bullets, Numbers.
        "html": true, // Button which allows you to edit the generated HTML.
        "link": true, // Button to insert a link.
        "image": true, // Button to insert an image.
        "color": false, // Button to change color of font
        "blockquote": true, // Blockquote
      }
    });
  });

  $('#member_translations_attributes_1_bio').each(function(i, elem) {
    $(elem).wysihtml5({
      toolbar: {
        "font-styles": true, // Font styling, e.g. h1, h2, etc.
        "emphasis": true, // Italics, bold, etc.
        "lists": true, // (Un)ordered lists, e.g. Bullets, Numbers.
        "html": true, // Button which allows you to edit the generated HTML.
        "link": true, // Button to insert a link.
        "image": true, // Button to insert an image.
        "color": false, // Button to change color of font
        "blockquote": true, // Blockquote
      }
    });
  });

  $('#member_translations_attributes_2_bio').each(function(i, elem) {
    $(elem).wysihtml5({
      toolbar: {
        "font-styles": true, // Font styling, e.g. h1, h2, etc.
        "emphasis": true, // Italics, bold, etc.
        "lists": true, // (Un)ordered lists, e.g. Bullets, Numbers.
        "html": true, // Button which allows you to edit the generated HTML.
        "link": true, // Button to insert a link.
        "image": true, // Button to insert an image.
        "color": false, // Button to change color of font
        "blockquote": true, // Blockquote
      }
    });
  });

});