function filterSelection(categoryName) {
  if (categoryName == 'all') {
    showAll();
  } else {
    showCategory(categoryName);
  }
}

function showCategory(categoryName){
  var all_stories = $('.filterDiv');
  $.each(all_stories, function(key, val) {
    $(val).addClass('d-none')
    if ($(val).hasClass(categoryName)) {
      $('.' + categoryName).removeClass('d-none');
    }
  })
}

function showAll(){
  var all_stories = $('.filterDiv');
  $.each(all_stories, function(key, val) {
    $(val).removeClass('d-none');
  })
}
