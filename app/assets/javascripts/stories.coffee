# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

# $('.grid').masonry({
#   // options...
#   itemSelector: '.grid-item',
#   columnWidth: 200
# });
$(document).on "turbolinks:load", ->
    $('.grid').masonry({
      itemSelector: '.grid-item',
    #   columnWidth: '.grid-sizer',
      columnWidth: 250,
    #   percentPosition: true
    });
return
