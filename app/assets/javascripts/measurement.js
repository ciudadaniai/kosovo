function loadMarkers(currId, categoryId, forRecommendation, themeId, forTheme) {
  $.ajax({
    url: '/initiatives.json',
    data: { measurement_id: currId, category_id: categoryId, for_recommendation: forRecommendation, theme_id: themeId, for_theme: forTheme },
    success: function(response) {
      resetMarkers(initiativeMarkers);
      initiativeMarkers = [];
      $.each(response, function(key, val) {
        var areas = val.get_areas;
        var color = val.get_color;
        var title = val.title;
        var id = val.id;
        $.each(areas, function(key, val) {
          console.log(initiativeMarkers);
          var ten_meters = 0.0001;
          var angle = Math.random() * Math.PI * 2;
          var length = Math.random() * ten_meters;
          var x_offset = Math.cos(angle) * length;
          var y_offset = Math.sin(angle) * length;
          var myLatLng = {lat: val.latitude + y_offset, lng: val.longitude + x_offset};
          var icon = {
            path: "M35,0C15.7,0,0,15.7,0,35c0,0.8,0,1.6,0.1,2.4c0,0.2,0,0.5,0.1,0.7c0,0.5,0.1,1,0.2,1.5C3.7,66.5,35,90,35,90	s31.3-23.5,34.7-50.4c0.1-0.5,0.1-1,0.2-1.5c0-0.2,0-0.5,0.1-0.7C70,36.6,70,35.8,70,35C70,15.7,54.3,0,35,0z M35,42.5	c-3.6,0-6.4-2.9-6.4-6.4c0-3.6,2.9-6.4,6.4-6.4s6.4,2.9,6.4,6.4C41.5,39.6,38.6,42.5,35,42.5z",
            fillColor: '#' + color,
            fillOpacity: .9,
            strokeWeight: 0,
            scale: 0.3,
          }
          var contentString = '<div id="content">'+
            '<div id="bodyContent">'+
            '<p><b>'+ title +'</b></p>'+
            '<p class="overview-tag align-right vr-grow" data-toggle="modal" data-target="#initiative'+ id +'Modal">' +
            I18n.t("measurements.map.read-more") + '</p>'+
            '</div>'+
            '</div>';

          var infowindow = new google.maps.InfoWindow({
            content: contentString
          });

          var marker = new google.maps.Marker({
            icon: icon,
            position: myLatLng,
            map: initiativeMap,
            title: val.initiative_location,
          });

          marker.addListener('click', function() {
            if (modalMarker){
              modalMarker.close();
            }
            infowindow.open(map, marker);
            modalMarker = infowindow;
          });

          initiativeMarkers.push(marker);
        })
      })
    }
  });
}

var modalMarker;
var initiativeMap;
var initiativeMarkers = [];
function initMap() {
  initiativeMap = new google.maps.Map(document.getElementById('map'), {
    center: {lat: 42.6026344, lng: 20.902977},
    zoom: 7.5,

    styles:
    [
      {
        "elementType": "labels.icon",
        "stylers": [
          {
            "visibility": "on"
          },
          {
            "weight": 6.5
          }
        ]
      },
      {
        "elementType": "labels.text.fill",
        "stylers": [
          {
            "color": "#616161"
          }
        ]
      },
      {
        "elementType": "labels.text.stroke",
        "stylers": [
          {
            "color": "#f5f5f5"
          }
        ]
      },
      {
        "featureType": "administrative.land_parcel",
        "elementType": "labels.text.fill",
        "stylers": [
          {
            "color": "#bdbdbd"
          }
        ]
      },
      {
        "featureType": "landscape",
        "elementType": "geometry",
        "stylers": [
          {
            "visibility": "off"
          }
        ]
      },
      {
        "featureType": "poi",
        "elementType": "labels.text.fill",
        "stylers": [
          {
            "color": "#757575"
          }
        ]
      },
      {
        "featureType": "poi.park",
        "elementType": "geometry",
        "stylers": [
          {
            "color": "#e5e5e5"
          }
        ]
      },
      {
        "featureType": "poi.park",
        "elementType": "labels.text.fill",
        "stylers": [
          {
            "color": "#9e9e9e"
          }
        ]
      },
      {
        "featureType": "road",
        "elementType": "geometry",
        "stylers": [
          {
            "color": "#ffffff"
          }
        ]
      },
      {
        "featureType": "road.arterial",
        "elementType": "labels",
        "stylers": [
          {
            "visibility": "off"
          }
        ]
      },
      {
        "featureType": "road.arterial",
        "elementType": "labels.text.fill",
        "stylers": [
          {
            "color": "#757575"
          }
        ]
      },
      {
        "featureType": "road.highway",
        "elementType": "geometry",
        "stylers": [
          {
            "color": "#dadada"
          }
        ]
      },
      {
        "featureType": "road.highway",
        "elementType": "labels",
        "stylers": [
          {
            "visibility": "off"
          }
        ]
      },
      {
        "featureType": "road.highway",
        "elementType": "labels.text.fill",
        "stylers": [
          {
            "color": "#616161"
          }
        ]
      },
      {
        "featureType": "road.local",
        "stylers": [
          {
            "visibility": "off"
          }
        ]
      },
      {
        "featureType": "road.local",
        "elementType": "labels.text.fill",
        "stylers": [
          {
            "color": "#9e9e9e"
          }
        ]
      },
      {
        "featureType": "transit.line",
        "elementType": "geometry",
        "stylers": [
          {
            "color": "#e5e5e5"
          }
        ]
      },
      {
        "featureType": "transit.station",
        "elementType": "geometry",
        "stylers": [
          {
            "color": "#eeeeee"
          }
        ]
      },
      {
        "featureType": "water",
        "stylers": [
          {
            "color": "#ff50af"
          },
          {
            "visibility": "on"
          },
          {
            "weight": 8
          }
        ]
      },
      {
        "featureType": "water",
        "elementType": "geometry",
        "stylers": [
          {
            "color": "#7ed0ba"
          }
        ]
      },
      {
        "featureType": "water",
        "elementType": "labels.text.fill",
        "stylers": [
          {
            "color": "#9e9e9e"
          }
        ]
      }
    ]
  });
  var currId = $('#js-measurement').data('id');
  var categoryId = $('#js-category-id').data('id');
  var forRecommendation = $('#js-for-recommendation').data('id');
  var themeId = $('#js-theme-id').data('id');
  var forTheme = $('#js-for-theme').data('id');
  loadMarkers(currId, categoryId, forRecommendation, themeId, forTheme);
}
function resetMarkers(arr) {
  if (arr && arr.length != 0) {
    $.each(arr, function (index, value) { value.setMap(null); });
  }
}
