source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '2.6.3'
gem 'bundler', '~> 2.0.2'
gem 'dotenv-rails'
# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 5.2.2', '>= 5.2.2.1'
# Use postgresql as the database for Active Record
gem 'pg', '>= 0.18', '< 2.0'
# Use Puma as the app server
gem 'puma', '~> 3.11'
# Use SCSS for stylesheets
gem 'sass-rails', '~> 5.0'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# See https://github.com/rails/execjs#readme for more supported runtimes
# gem 'mini_racer', platforms: :ruby

# Use CoffeeScript for .coffee assets and views
gem 'coffee-rails', '~> 4.2'
# Turbolinks makes navigating your web application faster. Read more: https://github.com/turbolinks/turbolinks
gem 'turbolinks', '~> 5'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.5'
# Use Redis adapter to run Action Cable in production
# gem 'redis', '~> 4.0'
# Use ActiveModel has_secure_password
gem 'bcrypt', '~> 3.1.7'
# gem 'bcrypt', '~> 3.1.7'
# Use Bootstrap
gem 'bootstrap', '~> 4.3.1'
gem 'jquery-rails'

# Use ActiveStorage variant
gem 'mini_magick', '>= 4.9.5'
gem "image_processing", "~> 1.0"

gem 'bootstrap-wysihtml5-rails'

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development

# Reduces boot times through caching; required in config/boot.rb
gem 'bootsnap', '>= 1.1.0', require: false
# Manejo de usuario e inicio de sesión
gem 'devise'
gem 'rails_admin', '~> 2.0'
gem 'cancancan', '~> 2.0'
gem 'paper_trail'
gem 'rails-i18n'
gem "i18n-js" # Helps us localize JS strings
gem 'active_storage_validations'
gem 'acts-as-taggable-on', '~> 6.0'
# Globalize builds on the I18n API in Ruby on Rails to add model translations to ActiveRecord models.
gem 'globalize', git: 'https://github.com/globalize/globalize'
gem 'globalize-accessors', '~> 0.2.1'
gem 'activemodel-serializers-xml'
gem 'rails_admin_globalize_field'

gem 'pg_search'
gem 'redcarpet'
gem 'kaminari'
gem 'bootstrap4-kaminari-views'

gem 'recaptcha'
# gem 'faker', :git => 'https://github.com/stympy/faker.git', :branch => 'master'

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug', platforms: [:mri, :mingw, :x64_mingw]
  gem "factory_bot_rails"
  gem "rspec"
  gem "rspec-rails"
  gem 'faker'
end

group :development do
  # Access an interactive console on exception pages or by calling 'console' anywhere in the code.
  gem 'web-console', '>= 3.3.0'
  gem 'listen', '>= 3.0.5', '< 3.2'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
  gem 'awesome_print'
end

group :test do
  # Adds support for Capybara system testing and selenium driver
  gem 'capybara', '>= 2.15'
  gem 'webdrivers', '~> 3.0'
  gem 'i18n-tasks'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
