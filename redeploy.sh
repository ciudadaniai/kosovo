#!/bin/bash
datetime_1=$(date)
user=$(whoami)
USER="ktb"
echo "$datetime_1 running redeploy.sh as $user" >> log/cd/redeploy.log

echo "              updating code from main repo: git pull" >> log/cd/redeploy.log
echo "su - $USER -c git pull >> log/cd/redeploy.log"
su - "$USER" -c "cd development && git pull" >> log/cd/redeploy.log

echo "              installing missing gems: bundle install" >> log/cd/redeploy.log
su - "$USER" -c "cd development && bundle install" >> log/cd/redeploy.log

echo "              syncronizing database rails db:migrate" >> log/cd/redeploy.log
su - "$USER" -c "cd development && RAILS_ENV=development rails db:migrate" >> log/cd/redeploy.log

echo "              reloading search tables: rake pg_search:multisearch:rebuild" >> log/cd/redeploy.log
su - "$USER" -c "cd development && RAILS_ENV=development rake pg_search:multisearch:rebuild[Initiative]" >> log/cd/redeploy.log
su - "$USER" -c "cd development && RAILS_ENV=development rake pg_search:multisearch:rebuild[Measurement]" >> log/cd/redeploy.log
su - "$USER" -c "cd development && RAILS_ENV=development rake pg_search:multisearch:rebuild[Story]" >> log/cd/redeploy.log
su - "$USER" -c "cd development && RAILS_ENV=development rake pg_search:multisearch:rebuild[Actor]" >> log/cd/redeploy.log

echo "              restarting server: service nginx restart" >> log/cd/redeploy.log
su - "$USER" -c "service nginx restart" >> log/cd/redeploy.log

datetime_2=$(date)
echo "      execution took $(( $(date -d "$date2" "+%s") - $(date -d "$date1" "+%s") )) seconds" >> log/cd/redeploy.log
