#!/bin/bash
datetime_1=$(date)
user=$(whoami)
USER="ktb"
ENV="production"
ENV_DIR="kosovo"
echo "$datetime_1 running redeploy.sh as $user" >> log/cd/redeploy.log

echo "              updating code from main repo: git pull" >> log/cd/redeploy.log
echo "su - $USER -c git pull >> log/cd/redeploy.log"
su - "$USER" -c "cd $ENV_DIR && git pull" >> log/cd/redeploy.log

echo "              installing missing gems: bundle install" >> log/cd/redeploy.log
su - "$USER" -c "cd $ENV_DIR && bundle install"

echo "              syncronizing database rails db:migrate" >> log/cd/redeploy.log
su - "$USER" -c "cd $ENV_DIR && RAILS_ENV=$ENV rails db:migrate"

echo "              precompiling assets" >> log/cd/redeploy.log
su - "$USER" -c "cd $ENV_DIR && RAILS_ENV=$ENV rails assets:precompile"

echo "              reloading search tables: rake pg_search:multisearch:rebuild" >> log/cd/redeploy.log
su - "$USER" -c "cd $ENV_DIR && RAILS_ENV=$ENV rake pg_search:multisearch:rebuild[Initiative]"
su - "$USER" -c "cd $ENV_DIR && RAILS_ENV=$ENV rake pg_search:multisearch:rebuild[Measurement]"
su - "$USER" -c "cd $ENV_DIR && RAILS_ENV=$ENV rake pg_search:multisearch:rebuild[Story]"
su - "$USER" -c "cd $ENV_DIR && RAILS_ENV=$ENV rake pg_search:multisearch:rebuild[Actor]"

echo "              restarting server: service nginx restart" >> log/cd/redeploy.log
su - "$USER" -c "service nginx restart"

datetime_2=$(date)
echo "      execution took $(( $(date -d "$date2" "+%s") - $(date -d "$date1" "+%s") )) seconds" >> log/cd/redeploy.log
