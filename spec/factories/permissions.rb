require "factory_bot"

FactoryBot.define do
  factory :permission do
    subject_class   { "a" }
    action          { "a" }
    name            { "a" }
    modifier        { "a" }
  end
end
