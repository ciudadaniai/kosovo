(function($){
    "use strict"; // Start of use strict


    /* ---------------------------------------------
     Scripts initialization
     --------------------------------------------- */

    $(document).ready(function(){

        $(window).trigger("resize");

        init_nbc_menu();
        init_classic_menu();
        init_lightbox();
        init_parallax();
        init_shortcodes();
        init_tooltips();
        init_counters();
        init_team();
        initPageSliders();
        initWorkFilter();
        init_services();
        init_google_map();

    });

    $(window).resize(function(){

        init_hipster_screen();
        init_nbc_menu_resize();
        init_classic_menu_resize();
        js_height_init();
        service_height_init();

    });


    /* --------------------------------------------
     Platform detect
     --------------------------------------------- */
    var mobileTest;
    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)) {
        mobileTest = true;
        $("html").addClass("mobile");
    }
    else {
        mobileTest = false;
        $("html").addClass("no-mobile");
    }

    var mozillaTest;
    if (/mozilla/.test(navigator.userAgent)) {
        mozillaTest = true;
    }
    else {
        mozillaTest = false;
    }
    var safariTest;
    if (/safari/.test(navigator.userAgent)) {
        safariTest = true;
    }
    else {
        safariTest = false;
    }

    // Detect touch devices
    if (!("ontouchstart" in document.documentElement)) {
        document.documentElement.className += " no-touch";
    }


    /* ---------------------------------------------
     Sections helpers
     --------------------------------------------- */

    // Sections backgrounds

    var pageSection = $(".home-section, .page-section, .small-section, .split-section");
    pageSection.each(function(indx){

        if ($(this).attr("data-background")){
            $(this).css("background-image", "url(" + $(this).data("background") + ")");
        }
    });

    // Function for block height 100%
    function height_line(height_object, height_donor){
        height_object.height(height_donor.height());
        height_object.css({
            "line-height": height_donor.height() + "px"
        });
    }

    // Function equal height
    !function(a){
        a.fn.equalHeights = function(){
            var b = 0, c = a(this);
            return c.each(function(){
                var c = a(this).innerHeight();
                c > b && (b = c)
            }), c.css("height", b)
        }, a("[data-equal]").each(function(){
            var b = a(this), c = b.data("equal");
            b.find(c).equalHeights()
        })
    }(jQuery);


    /* --------------------------------------------
     Header "Hipster Style"
     --------------------------------------------- */
    var hsCont = $(".hs-cont");
    var hsWrap = $(".hs-wrap");
    var hsLine2 = $(".js-hs-line-2");
    var hsLine2Mar;

     function init_hipster_screen(){
        hsLine2Mar = (hsCont.width() - hsWrap.width()) / 2;

        hsLine2.css({
            marginLeft: -hsLine2Mar,
            marginRight: -hsLine2Mar
        });

    }

    /* ---------------------------------------------
     Nav panel compact
     --------------------------------------------- */

    function init_nbc_menu_resize(){
       $(".nbc-menu-wrap").css("max-height", $(window).height() - $(".nav-bar-compact").height() - 20 + "px");
    }

    var nbc_menu_button = $(".nbc-menu-button");
    var nbc_menu_wrap = $(".nbc-menu-wrap");

    function init_nbc_menu(){

        nbc_menu_button.click(function(){

            if ($(this).hasClass("js-active")) {

                $(this).removeClass("js-active");
                $(".nav-bar-compact").removeClass("js-opened");

                setTimeout(function(){
                    nbc_menu_wrap.hide();
                }, 200);


            }
            else {

                $(this).addClass("js-active");
                nbc_menu_wrap.show();

                setTimeout(function(){
                    $(".nav-bar-compact").addClass("js-opened");
                }, 50);

            }

        });

        nbc_menu_wrap.find("a:not(.nbc-has-sub)").click(function(){

            if (nbc_menu_button.hasClass("js-active")) {

                nbc_menu_button.removeClass("js-active");
                $(".nav-bar-compact").removeClass("js-opened");

            }
        });


        // BG after scroll

        $(window).scroll(function(){

            if ($(window).scrollTop() >= 100) {
                $(".nav-bar-compact").addClass("js-nbc-bg");
            }
            else {
                $(".nav-bar-compact").removeClass("js-nbc-bg");
            }

        });

    }


    /* ---------------------------------------------
     Nav panel classic
     --------------------------------------------- */

    var mobile_nav = $(".mobile-nav");
    var desktop_nav = $(".desktop-nav");

    function init_classic_menu_resize(){

        // Mobile menu max height
        $(".mobile-on .desktop-nav > ul").css("max-height", $(window).height() - $(".main-nav").height() - 20 + "px");

        // Mobile menu style toggle
        if ($(window).width() < 1024) {
            $(".main-nav").addClass("mobile-on");
        }
        else
            if ($(window).width() >= 1024) {
                $(".main-nav").removeClass("mobile-on");
                desktop_nav.show();
            }
    }

    function init_classic_menu(){

        height_line($(".nav-logo-wrap .logo"), $(".main-nav"));

        // Navbar sticky

        $(".js-stick").sticky({
            topSpacing: 0
        });


        height_line($(".inner-nav > ul > li > a"), $(".main-nav"));
        height_line(mobile_nav, $(".main-nav"));

        mobile_nav.css({
            "width": $(".main-nav").height() + "px"
        });


        // Mobile menu toggle

        mobile_nav.click(function(){

            if (desktop_nav.hasClass("js-opened")) {
                desktop_nav.slideUp("slow", "easeOutExpo").removeClass("js-opened");
                $(this).removeClass("active");
            }
            else {
                desktop_nav.slideDown("slow", "easeOutQuart").addClass("js-opened");
                $(this).addClass("active");
            }

        });

        desktop_nav.find("a:not(.mn-has-sub)").click(function(){
            if (mobile_nav.hasClass("active")) {
                desktop_nav.slideUp("slow", "easeOutExpo").removeClass("js-opened");
                mobile_nav.removeClass("active");
            }
        });


        // Sub menu


        var mnHasSub = $(".mn-has-sub");
        var mnThisLi;

        $(".mobile-on .mn-has-sub").find(".fa:first").removeClass("fa-angle-right").addClass("fa-angle-down");

        mnHasSub.click(function(){

            if ($(".main-nav").hasClass("mobile-on")) {
                mnThisLi = $(this).parent("li:first");
                if (mnThisLi.hasClass("js-opened")) {
                    mnThisLi.find(".mn-sub:first").slideUp(function(){
                        mnThisLi.removeClass("js-opened");
                        mnThisLi.find(".mn-has-sub").find(".fa:first").removeClass("fa-angle-up").addClass("fa-angle-down");
                    });
                }
                else {
                    $(this).find(".fa:first").removeClass("fa-angle-down").addClass("fa-angle-up");
                    mnThisLi.addClass("js-opened");
                    mnThisLi.find(".mn-sub:first").slideDown();
                }

                return false;
            }
            else {
                return false;
            }

        });

        mnThisLi = mnHasSub.parent("li");
        mnThisLi.hover(function(){

            if (!($(".main-nav").hasClass("mobile-on"))) {

                $(this).find(".mn-sub:first").stop(true, true).fadeIn("fast");
            }

        }, function(){

            if (!($(".main-nav").hasClass("mobile-on"))) {

                $(this).find(".mn-sub:first").stop(true, true).delay(100).fadeOut("fast");
            }

        });

    }



    /* ---------------------------------------------
     Team
     --------------------------------------------- */

    function init_team(){

        // Hover
        $(".team-item").click(function(){
            if ($("html").hasClass("mobile")) {
                $(this).toggleClass("js-active");
            }
        });

    }


})(jQuery); // End of use strict




/* ---------------------------------------------
 Height 100%
 --------------------------------------------- */
function js_height_init(){
    (function($){
        $(".js-height-full").height($(window).height());
        $(".js-height-parent").each(function(){
            $(this).height($(this).parent().first().height());
        });
    })(jQuery);
}
