#!/bin/bash
datetime_1=$(date)
user=$(whoami)
USER="ktb"
ENV="staging"
ENV_DIR="kosovo_stag"
echo "$datetime_1 running redeploy.sh as $user" >> log/cd/redeploy.log

echo "              updating code from main repo: git pull" >> log/cd/redeploy.log
echo "su - $USER -c git pull >> log/cd/redeploy.log"
su - "$USER" -c "cd $ENV_DIR && git pull" >> log/cd/redeploy.log

echo "              installing missing gems: bundle install" >> log/cd/redeploy.log
su - "$USER" -c "cd $ENV_DIR && bundle install"

echo "              syncronizing database rails db:migrate" >> log/cd/redeploy.log
su - "$USER" -c "cd $ENV_DIR && RAILS_ENV=$ENV rails db:migrate"

echo "              precompiling assets" >> log/cd/redeploy.log
su - "$USER" -c "cd $ENV_DIR && RAILS_ENV=$ENV rails assets:precompile"



datetime_2=$(date)
echo "      execution took $(( $(date -d "$date2" "+%s") - $(date -d "$date1" "+%s") )) seconds" >> log/cd/redeploy.log
