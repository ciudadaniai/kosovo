#!/bin/bash
datetime=$(date)
user=$(whoami)
ENV=$(git status | grep "On branch" | awk '{ print $3}')

if [ "$ENV" == "master" ]
then
    envs = (production)
else
    envs = (development staging)
fi

for i in "${envs[@]}"; do
    
    ENV="$i"

    echo "running rebuild_search.sh for $ENV environment" >> log/rebuild.log
    echo "$datetime running rebuild_search.sh as $user" >> log/rebuild.log

    echo "RAILS_ENV=$ENV rake pg_search:multisearch:rebuild[Story] --trace" >> log/rebuild.log 2>&1
    RAILS_ENV=$ENV rake pg_search:multisearch:rebuild[Story] --trace >> log/rebuild.log 2>&1

    echo "RAILS_ENV=$ENV rake pg_search:multisearch:rebuild[Initiative] --trace" >> log/rebuild.log 2>&1
    RAILS_ENV=$ENV rake pg_search:multisearch:rebuild[Initiative] --trace >> log/rebuild.log 2>&1

    echo "RAILS_ENV=$ENV rake pg_search:multisearch:rebuild[Measurement] --trace" >> log/rebuild.log 2>&1
    RAILS_ENV=$ENV rake pg_search:multisearch:rebuild[Measurement] --trace >> log/rebuild.log 2>&1

    datetime=$(date)
    echo "      rebuild_search.sh finished execution at $datetime" >> log/rebuild.log

done