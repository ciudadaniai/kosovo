# Traducciones

## Usar traducciones

### Nombre del modelo
* Para usar la forma singular: `Model.model_name.human`
* Para usar la forma plural:` Model.model_name.human(count: 2)`

Ejemplo: `<h2> <%= Measurement.model_name.human(count: 2) %> </h2>`

### Atributo de un modelo
Se usa `Model.human_attribute_name("nombre del atributo")`

Ejemplo: `<h3> <%= Measurement.human_attribute_name("title") %> </h3>`


## Agregar traducciones

Se deben agregar en el archivo correspondiente al idioma (en, sq, sr) en /config/locales.
