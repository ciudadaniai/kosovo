RailsAdmin.config do |config|

  ### Nombre del Admin Panel
  config.main_app_name = ['Kosovo', 'Admin']

  ### Popular gems integration
  config.parent_controller = 'ApplicationController'

  config.navigation_static_links = {
    'English' => '/en/admin',
    'Shqip' => '/sq/admin',
    'Srpski' => '/sr/admin',
  }

  config.default_associated_collection_limit = 400

  ## == Devise ==
  config.authenticate_with do
    warden.authenticate! scope: :user
  end
  config.current_user_method(&:current_user)

  ## == Cancan ==
  config.authorize_with :cancancan

  ## == PaperTrail ==
  config.audit_with :paper_trail, 'User', 'PaperTrail::Version' # PaperTrail >= 3.0.0

  config.included_models = [
    'CarouselSlot',
    'CarouselSlot::Translation',
    'Initiative',
    'InitiativeStatus',
    'Measurement',
    'MeasurementCategory',
    'Member',
    'MemberList',
    'Permission',
    'Role',
    'Story',
    'StoryCategory',
    'Actor',
    'Tag',
    'User',
    'Area',
    'MemberLink',
    'CategoryLink',
    'Measurement::Translation',
    'Area::Translation',
    'Actor::Translation',
    'MeasurementCategory::Translation',
    'Member::Translation',
    'MemberList::Translation',
    'Initiative::Translation',
    'InitiativeStatus::Translation',
    'Story::Translation',
    'StoryCategory::Translation',
    'Event']

  ### More at https://github.com/sferik/rails_admin/wiki/Base-configuration

  config.actions do
    dashboard                     # mandatory
    index                         # mandatory
    new
    export
    bulk_delete
    show
    edit
    delete
    show_in_app

    ## With an audit adapter, you can add:
    history_index
    history_show
  end

  config.model 'Tag' do
    edit do
      field :name
    end
  end

  config.model 'CarouselSlot' do
    configure :translations, :globalize_tabs
    list do
      field :id
      field :carousel_text
      field :link
      field :link_text
      field :visible
      field :ordering
      field :picture, :active_storage
    end

    edit do
      include_all_fields
      field :picture, :active_storage do
        required true
      end
    end
  end

  config.model 'CarouselSlot::Translation' do
    visible false
    configure :locale, :hidden do
      help ''
    end
    include_fields :locale, :carousel_text, :link_text
  end

  config.model 'User' do
    list do
      field :id
      field :email
      field :name
    end
    edit do
      field :email
      field :password
      field :password_confirmation
      field :name
      field :role
    end
  end

  config.model 'Role' do
    list do
      field :id
      field :name
      field :users
    end
  end

  config.model 'Event' do
    configure :startdate, :date do
      date_format :default
    end
    configure :enddate, :date do
      date_format :default
    end
    configure :starttime, :time do
      date_format :default
    end
    configure :endtime, :time do
      date_format :default
    end
    configure :color, :color
    list do
      include_all_fields
    end
  end

  config.model 'Actor' do
    configure :translations, :globalize_tabs
    list do
      search_by :search_by_name
      field :id
      field :name
    end
  end

  config.model 'Actor::Translation' do
    visible false
    configure :locale, :hidden do
      help ''
    end
    include_fields :locale, :name
  end

  config.model 'Area' do
    configure :translations, :globalize_tabs
    list do
      search_by :search_by_name
      field :id
      field :name
      field :latitude
      field :longitude
    end
  end

  config.model 'Area::Translation' do
    visible false
    configure :locale, :hidden do
      help ''
    end
    include_fields :locale, :name
  end

  config.model 'Measurement' do
    configure :translations, :globalize_tabs
    configure :tags # Very important to allow for tag edition
    list do
      search_by :search_by_title
      field :id
      field :title
      field :measurement_category
      field :sdg
      items_per_page 100
    end
  end

  config.model 'Measurement::Translation' do
    visible false
    configure :locale, :hidden do
      help ''
    end
    include_fields :locale, :title, :summary, :description
  end

  config.model 'MeasurementCategory' do
    list do
      field :id
      field :name
    end
    configure :translations, :globalize_tabs
  end

  config.model 'MeasurementCategory::Translation' do
    visible false
    configure :locale, :hidden do
      help ''
    end
    include_fields :locale, :name, :description
  end

  config.model 'Story' do
    configure :translations, :globalize_tabs
    configure :tags # Very important to allow for tag edition
    list do
      search_by :search_by_title
      field :id
      field :title
      field :story_category
    end
  end

  config.model 'Story::Translation' do
    visible false
    configure :locale, :hidden do
      help ''
    end
    include_fields :locale, :title, :summary, :content
  end

  config.model 'Initiative' do
    configure :translations, :globalize_tabs
    configure :tags # Very important to allow for tag edition
    list do
      search_by :search_by_title
      field :id
      field :title
      field :description
      field :measurement_category
    end
    field :measurement
    include_all_fields
    exclude_fields :measurement_category
  end

  config.model 'Initiative::Translation' do
    visible false
    configure :locale, :hidden do
      help ''
    end
    include_fields :locale, :title, :summary, :description
  end

  config.model 'StoryCategory' do
    configure :translations, :globalize_tabs
    list do
      field :id
      field :title
    end
  end

  config.model 'StoryCategory::Translation' do
    visible false
    configure :locale, :hidden do
      help ''
    end
    include_fields :locale, :title, :description
  end

  config.model 'Member' do
    configure :translations, :globalize_tabs
    list do
      field :id
      field :name
      field :member_list
      field :bio
    end
  end

  config.model 'Member::Translation' do
    visible false
    configure :locale, :hidden do
      help ''
    end
    include_fields :locale, :bio
  end

  config.model 'MemberList' do
    configure :translations, :globalize_tabs
    list do
      field :id
      field :name
      field :members
    end
  end

  config.model 'MemberList::Translation' do
    visible false
    configure :locale, :hidden do
      help ''
    end
    include_fields :locale, :name
  end

  config.model 'InitiativeStatus' do
    configure :translations, :globalize_tabs
    list do
      field :id
      field :status
      field :created_at
    end
    field :color, :color
    include_all_fields
  end

  config.model 'InitiativeStatus::Translation' do
    visible false
    configure :locale, :hidden do
      help ''
    end
    include_fields :locale, :status, :description
  end
end
