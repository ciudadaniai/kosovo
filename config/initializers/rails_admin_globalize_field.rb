RailsAdminGlobalizeField::Tab.class_eval do
    def label
        I18n.translate("locale_name.#{locale}")
    end
end