Rails.application.routes.draw do
  scope "(:locale)", locale: /en|sr|sq/  do
    post 'measurements/search'
    get 'measurements/search_all', action: :search_all, controller: :measurements
    post 'measurements/search_all', action: :search_all, controller: :measurements

    resources :stories
    resources :measurements do
      get :by_location, on: :collection
    end
    resources :initiatives, only: :index
    resources :measurements_categories
    resources :about, only: [:index]
    resources :actors, only: [:show]
    resources :resources, only: [:index]
    resources :covid19, only: [:index]

    post "/contact" => "home#contact"
    get "/start" => "home#start"
    get "/recommendations" => "home#recommendations"
    get "/contact" => "home#contact"
    root to: "home#index"
    mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  end

  resources :events do
    get 'events_json', on: :collection
  end

  devise_for :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
