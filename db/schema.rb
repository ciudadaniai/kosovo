# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_04_08_134319) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "active_storage_attachments", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.bigint "blob_id", null: false
    t.datetime "created_at", null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.bigint "byte_size", null: false
    t.string "checksum", null: false
    t.datetime "created_at", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "actor_translations", force: :cascade do |t|
    t.bigint "actor_id", null: false
    t.string "locale", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "name"
    t.index ["actor_id"], name: "index_actor_translations_on_actor_id"
    t.index ["locale"], name: "index_actor_translations_on_locale"
  end

  create_table "actors", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "actors_initiatives", id: false, force: :cascade do |t|
    t.bigint "actor_id", null: false
    t.bigint "initiative_id", null: false
  end

  create_table "actors_stories", id: false, force: :cascade do |t|
    t.bigint "actor_id", null: false
    t.bigint "story_id", null: false
  end

  create_table "area_translations", force: :cascade do |t|
    t.bigint "area_id", null: false
    t.string "locale", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "name"
    t.index ["area_id"], name: "index_area_translations_on_area_id"
    t.index ["locale"], name: "index_area_translations_on_locale"
  end

  create_table "areas", force: :cascade do |t|
    t.string "name"
    t.float "latitude"
    t.float "longitude"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "areas_initiatives", id: false, force: :cascade do |t|
    t.bigint "area_id", null: false
    t.bigint "initiative_id", null: false
  end

  create_table "carousel_slot_translations", force: :cascade do |t|
    t.bigint "carousel_slot_id", null: false
    t.string "locale", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "link_text"
    t.string "carousel_text"
    t.index ["carousel_slot_id"], name: "index_carousel_slot_translations_on_carousel_slot_id"
    t.index ["locale"], name: "index_carousel_slot_translations_on_locale"
  end

  create_table "carousel_slots", force: :cascade do |t|
    t.string "link_text", limit: 20
    t.string "link", null: false
    t.string "carousel_text", limit: 100
    t.boolean "visible", default: false, null: false
    t.integer "ordering"
  end

  create_table "category_links", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "events", force: :cascade do |t|
    t.string "name"
    t.datetime "startdate"
    t.datetime "enddate"
    t.datetime "starttime"
    t.datetime "endtime"
    t.string "color"
    t.string "url"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "initiative_status_translations", force: :cascade do |t|
    t.bigint "initiative_status_id", null: false
    t.string "locale", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "status"
    t.text "description"
    t.index ["initiative_status_id"], name: "index_initiative_status_translations_on_initiative_status_id"
    t.index ["locale"], name: "index_initiative_status_translations_on_locale"
  end

  create_table "initiative_statuses", force: :cascade do |t|
    t.string "status"
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "color"
  end

  create_table "initiative_translations", force: :cascade do |t|
    t.bigint "initiative_id", null: false
    t.string "locale", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "title"
    t.text "summary"
    t.text "description"
    t.index ["initiative_id"], name: "index_initiative_translations_on_initiative_id"
    t.index ["locale"], name: "index_initiative_translations_on_locale"
  end

  create_table "initiatives", force: :cascade do |t|
    t.string "title"
    t.text "summary"
    t.text "description"
    t.date "start_date"
    t.date "end_date"
    t.string "link"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "measurement_id"
    t.bigint "initiative_status_id"
    t.index ["initiative_status_id"], name: "index_initiatives_on_initiative_status_id"
    t.index ["measurement_id"], name: "index_initiatives_on_measurement_id"
  end

  create_table "measurement_categories", force: :cascade do |t|
    t.string "name"
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "measurement_category_translations", force: :cascade do |t|
    t.bigint "measurement_category_id", null: false
    t.string "locale", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "name"
    t.text "description"
    t.index ["locale"], name: "index_measurement_category_translations_on_locale"
    t.index ["measurement_category_id"], name: "index_8f3c22b46181ae272cee8a2c18bd09e323e72184"
  end

  create_table "measurement_translations", force: :cascade do |t|
    t.bigint "measurement_id", null: false
    t.string "locale", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "title"
    t.text "summary"
    t.text "description"
    t.index ["locale"], name: "index_measurement_translations_on_locale"
    t.index ["measurement_id"], name: "index_measurement_translations_on_measurement_id"
  end

  create_table "measurements", force: :cascade do |t|
    t.string "title"
    t.text "summary"
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "measurement_category_id"
    t.boolean "has_fulfillment"
    t.float "fulfillment"
    t.string "sdg"
    t.index ["measurement_category_id"], name: "index_measurements_on_measurement_category_id"
    t.index ["sdg"], name: "index_measurements_on_sdg"
  end

  create_table "member_links", force: :cascade do |t|
    t.string "link"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "member_id"
    t.bigint "category_link_id"
    t.index ["category_link_id"], name: "index_member_links_on_category_link_id"
    t.index ["member_id"], name: "index_member_links_on_member_id"
  end

  create_table "member_list_translations", force: :cascade do |t|
    t.bigint "member_list_id", null: false
    t.string "locale", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "name"
    t.index ["locale"], name: "index_member_list_translations_on_locale"
    t.index ["member_list_id"], name: "index_member_list_translations_on_member_list_id"
  end

  create_table "member_lists", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "category"
    t.boolean "visible"
  end

  create_table "member_translations", force: :cascade do |t|
    t.bigint "member_id", null: false
    t.string "locale", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "bio"
    t.index ["locale"], name: "index_member_translations_on_locale"
    t.index ["member_id"], name: "index_member_translations_on_member_id"
  end

  create_table "members", force: :cascade do |t|
    t.string "name"
    t.text "bio"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "member_list_id"
    t.index ["member_list_id"], name: "index_members_on_member_list_id"
  end

  create_table "members_stories", id: false, force: :cascade do |t|
    t.bigint "member_id", null: false
    t.bigint "story_id", null: false
  end

  create_table "permissions", force: :cascade do |t|
    t.string "subject_class"
    t.string "action"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "name"
    t.string "modifier"
  end

  create_table "permissions_roles", force: :cascade do |t|
    t.bigint "permission_id"
    t.bigint "role_id"
    t.index ["permission_id"], name: "index_permissions_roles_on_permission_id"
    t.index ["role_id"], name: "index_permissions_roles_on_role_id"
  end

  create_table "pg_search_documents", force: :cascade do |t|
    t.text "content"
    t.string "searchable_type"
    t.bigint "searchable_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["searchable_type", "searchable_id"], name: "index_pg_search_documents_on_searchable_type_and_searchable_id"
  end

  create_table "roles", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "stories", force: :cascade do |t|
    t.string "title"
    t.text "summary"
    t.text "content"
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "story_category_id"
    t.boolean "highlight", default: false
    t.index ["story_category_id"], name: "index_stories_on_story_category_id"
    t.index ["user_id"], name: "index_stories_on_user_id"
  end

  create_table "story_categories", force: :cascade do |t|
    t.string "title"
    t.string "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "story_category_translations", force: :cascade do |t|
    t.bigint "story_category_id", null: false
    t.string "locale", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "title"
    t.string "description"
    t.index ["locale"], name: "index_story_category_translations_on_locale"
    t.index ["story_category_id"], name: "index_story_category_translations_on_story_category_id"
  end

  create_table "story_translations", force: :cascade do |t|
    t.bigint "story_id", null: false
    t.string "locale", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "title"
    t.text "summary"
    t.text "content"
    t.index ["locale"], name: "index_story_translations_on_locale"
    t.index ["story_id"], name: "index_story_translations_on_story_id"
  end

  create_table "taggings", id: :serial, force: :cascade do |t|
    t.integer "tag_id"
    t.string "taggable_type"
    t.integer "taggable_id"
    t.string "tagger_type"
    t.integer "tagger_id"
    t.string "context", limit: 128
    t.datetime "created_at"
    t.index ["context"], name: "index_taggings_on_context"
    t.index ["tag_id", "taggable_id", "taggable_type", "context", "tagger_id", "tagger_type"], name: "taggings_idx", unique: true
    t.index ["tag_id"], name: "index_taggings_on_tag_id"
    t.index ["taggable_id", "taggable_type", "context"], name: "index_taggings_on_taggable_id_and_taggable_type_and_context"
    t.index ["taggable_id", "taggable_type", "tagger_id", "context"], name: "taggings_idy"
    t.index ["taggable_id"], name: "index_taggings_on_taggable_id"
    t.index ["taggable_type"], name: "index_taggings_on_taggable_type"
    t.index ["tagger_id", "tagger_type"], name: "index_taggings_on_tagger_id_and_tagger_type"
    t.index ["tagger_id"], name: "index_taggings_on_tagger_id"
  end

  create_table "tags", id: :serial, force: :cascade do |t|
    t.string "name"
    t.integer "taggings_count", default: 0
    t.index ["name"], name: "index_tags_on_name", unique: true
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.string "name"
    t.string "text"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string "unconfirmed_email"
    t.integer "failed_attempts", default: 0, null: false
    t.string "unlock_token"
    t.datetime "locked_at"
    t.bigint "role_id"
    t.index ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
    t.index ["role_id"], name: "index_users_on_role_id"
    t.index ["unlock_token"], name: "index_users_on_unlock_token", unique: true
  end

  create_table "versions", force: :cascade do |t|
    t.string "item_type", null: false
    t.integer "item_id", null: false
    t.string "event", null: false
    t.string "whodunnit"
    t.text "object"
    t.datetime "created_at"
    t.text "object_changes"
    t.index ["item_type", "item_id"], name: "index_versions_on_item_type_and_item_id"
  end

  add_foreign_key "active_storage_attachments", "active_storage_blobs", column: "blob_id"
  add_foreign_key "initiatives", "initiative_statuses"
  add_foreign_key "initiatives", "measurements"
  add_foreign_key "measurements", "measurement_categories"
  add_foreign_key "member_links", "category_links"
  add_foreign_key "member_links", "members"
  add_foreign_key "members", "member_lists"
  add_foreign_key "stories", "story_categories"
  add_foreign_key "stories", "users"
  add_foreign_key "users", "roles"
end
