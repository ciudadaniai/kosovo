# Rails.application.load_tasks # <---
require 'faker'
require 'csv'
require 'date'

Rake::Task['ktb:permissions'].invoke

#the highest role with all the permissions.
Role.find_or_create_by!(:name => "admin")

#other role
Role.find_or_create_by!(:name => "staff")

#other role
Role.find_or_create_by!(:name => "author")

#assign author the permission to manage all the stories
role = Role.find_by_name('author')
role.permissions << Permission.find_or_create_by!(:subject_class => 'Story', :action => "read", :modifier => "user_id: user.id", :name => "Story:read OWN")
role.permissions << Permission.find_or_create_by!(:subject_class => 'Story', :action => "create", :name => "Story:create")
role.permissions << Permission.find_or_create_by!(:subject_class => 'Story', :action => "edit", :modifier => "user_id: user.id", :name => "Story:edit OWN")
role.permissions << Permission.find_or_create_by!(:subject_class => "rails_admin", :action => "access", :name => "rails_admin:access")
role.permissions << Permission.find_or_create_by!(:subject_class => "dashboard", :action => "read", :name => "dashboard:read")
role.permissions << Permission.find_or_create_by!(:subject_class => "Tag", :action => "read", :name => "Tag:read")
role.permissions << Permission.find_or_create_by!(:subject_class => "Tag", :action => "create", :name => "Tag:create")
role.permissions << Permission.find_or_create_by!(:subject_class => "StoryCategory", :action => "read", :name => "StoryCategory:read")

#assign staff the permission to manage all the initiatives
role = Role.find_by_name('staff')
role.permissions << Permission.find_or_create_by!(:subject_class => 'Actor', :action => "read", :name => "Actor:read")
role.permissions << Permission.find_or_create_by!(:subject_class => 'Area', :action => "read", :name => "Area:read")
role.permissions << Permission.find_or_create_by!(:subject_class => 'Measurement', :action => "read", :name => "Measurement:read")
role.permissions << Permission.find_or_create_by!(:subject_class => 'InitiativeStatus', :action => "read", :name => "InitiativeStatus:read")
role.permissions << Permission.find_or_create_by!(:subject_class => 'Tag', :action => "read", :name => "Tag:read")
role.permissions << Permission.find_or_create_by!(:subject_class => 'Initiative', :action => "manage", :name => "Initiative:manage")
role.permissions << Permission.find_or_create_by!(:subject_class => "rails_admin", :action => "access", :name => "rails_admin:access")
role.permissions << Permission.find_or_create_by!(:subject_class => "dashboard", :action => "read", :name => "dashboard:read")
role.permissions << Permission.find_or_create_by!(:subject_class => "CarouselSlot", :action => "read", :name => "CarouselSlot:read")
role.permissions << Permission.find_or_create_by!(:subject_class => "CarouselSlot", :action => "edit", :name => "CarouselSlot:edit")


#assign super admin the permission to manage all the models and controllers
role = Role.find_by_name('admin')
#create a universal permission
role.permissions << Permission.find_or_create_by!(:subject_class => "all", :action => "manage", :name => "all:manage")

# create a user and assign the super admin role to him.
user = User.create(:name => "admin", :email => "ktb@ciudadaniai.org", :password => "adminx", :password_confirmation => "adminx")
user.role = role
user.skip_confirmation!
# Should not affect vaildations
user.save!

user = User.create(:name => "staff", :email => "staff@ciudadaniai.org", :password => "staffx", :password_confirmation => "staffx", :role_id => Role.find_by_name('staff').id)
user.skip_confirmation!
# Should not affect vaildations
user.save!
author1 = User.create(:name => "author1", :email => "author1@ciudadaniai.org", :password => "author1", :password_confirmation => "author1", :role_id => Role.find_by_name('author').id)
author1.skip_confirmation!
# Should not affect vaildations
author1.save!
author2 = User.create(:name => "author2", :email => "author2@ciudadaniai.org", :password => "author2", :password_confirmation => "author2", :role_id => Role.find_by_name('author').id)
author2.skip_confirmation!
# Should not affect vaildations
author2.save!
user.skip_confirmation!
# Should not affect vaildations
user.save!
p "Users ✅"

# Checked
news = StoryCategory.create!(
  :title_en => "News",
  :title_sr => "Vesti",
  :title_sq => "Lajme",
  :description => ""
)
videos = StoryCategory.create!(
  :title_en => "Video",
  :title_sr => "Video",
  :title_sq => "Video",
  :description => ""
)
blog = StoryCategory.create!(
  :title_en => "Blog",
  :title_sr => "Blog",
  :title_sq => "Blogu",
  :description => ""
)
champions = StoryCategory.create!(
  :title_en => "Champions",
  :title_sr => "Champions",
  :title_sq => "Champions",
  :description => ""
)

p "Story categories ok✅"

# Checked
Actor.create!(
  name_en: "Central institutions",
  name_sr: "Centralne institucije",
  name_sq: "Institucionet qendrore"
)

Actor.create!(
  name_en: "Local institutions",
  name_sr: "Lokalne institucije",
  name_sq: "Institucionet lokale"
)

Actor.create!(
  name_en: "International organizations",
  name_sr: "Međunarodne organizacije",
  name_sq: "Organizatat ndërkombëtare"
)

Actor.create!(
  name_en: "Civil Society",
  name_sr: "Civilno društvo",
  name_sq: "Shoqëria civile"
)

p "Actors ok ✅"

I18n.locale = :en

# Checked
facebook = CategoryLink.create(name: 'facebook')
facebook.icon.attach(io: File.open(File.join(Rails.root, "/app/assets/images/seed/facebook.png")), filename: "facebook")
twitter = CategoryLink.create(name: 'twitter')
twitter.icon.attach(io: File.open(File.join(Rails.root, "/app/assets/images/seed/twitter.png")), filename: "twitter")
instagram = CategoryLink.create(name: 'instagram')
instagram.icon.attach(io: File.open(File.join(Rails.root, "/app/assets/images/seed/instagram.png")), filename: "instagram")
generic = CategoryLink.create(name: 'generic')
generic.icon.attach(io: File.open(File.join(Rails.root, "/app/assets/images/seed/generic.png")), filename: "generic")
linkedin = CategoryLink.create(name: 'linkedin')
linkedin.icon.attach(io: File.open(File.join(Rails.root, "/app/assets/images/seed/linkedin.png")), filename: "linkedin")
youtube = CategoryLink.create(name: 'youtube')
youtube.icon.attach(io: File.open(File.join(Rails.root, "/app/assets/images/seed/youtube.png")), filename: "youtube")
whatsapp = CategoryLink.create(name: 'whatsapp')
whatsapp.icon.attach(io: File.open(File.join(Rails.root, "/app/assets/images/seed/whatsapp.png")), filename: "whatsapp")

p "CategoryLinks ✅"

# Checked
board = MemberList.create!(
  name_en: "Advisory Council", 
  name_sr: "Savetodavni odbor", 
  name_sq: "Këshilli këshillor", 
  category:'board', 
  visible: true)
contributors = MemberList.create!(
  name_en: "Contributors", 
  name_sr: "Saradnici", 
  name_sq: "Kontribuesit", 
  category: 'contributors', 
  visible: true)
sponsors = MemberList.create!(
  name_en: "Supported by", 
  name_sr: "Podržano od strane", 
  name_sq: "Mbështetur nga", 
  category: 'sponsors', 
  visible: true)
author = MemberList.create!(
  name_en: "Authors", 
  name_sr: "Author", 
  name_sq: "Author", 
  category: 'authors', 
  visible: false)

p "MemberList ✅"

I18n.locale = :en

# Read csv with members
csv_text = File.read(Rails.root.join('lib', 'seeds', 'members.csv'))
csv = CSV.parse(csv_text, :headers => true, :encoding => 'ISO-8859-1')
csv.each do |row|
  m = Member.new
  I18n.locale = :en
  m.name = row['name_en']
  m.bio = row['bio_en']
  m.member_list = eval(row['category'])
  I18n.locale = :sq
  m.bio = row['bio_sq']
  I18n.locale = :sr
  m.bio = row['bio_sr']
  
  # Checked
  m.save!
  picture = row['name_image']
  if !picture.nil?
    m.picture.attach(io: File.open(File.join(Rails.root, "/app/assets/images/seed/"+ picture)), filename: picture)
  end
  m_email = row['email']
  if !m_email.nil?
    MemberLink.create(category_link: generic, link:"mailto:"+m_email, member: m)
  end
  m_facebook = row['facebook']
  if !m_facebook.nil?
    MemberLink.create(category_link: facebook, link: m_facebook, member: m)
  end
  m_twitter = row['twitter']
  if !m_twitter.nil?
    MemberLink.create(category_link: twitter, link: m_twitter, member: m)
  end
  m_web = row['web']
  if !m_web.nil?
    MemberLink.create(category_link: generic, link: m_web, member: m)
  end
  m_linkedin = row['linkedin']
  if !m_linkedin.nil?
    MemberLink.create(category_link: linkedin, link: m_linkedin, member: m)
  end
  m_instagram = row['instagram']
  if !m_instagram.nil?
    MemberLink.create(category_link: instagram, link: m_instagram, member: m)
  end
  m_youtube = row['youtube']
  if !m_youtube.nil?
    MemberLink.create(category_link: youtube, link: m_youtube, member: m)
  end
end

p "Members ✅"

20.times do |tag|
  I18n.locale = :en
  t = Tag.create(name: Faker::Lorem.sentence(word_count: 1, supplemental: false, random_words_to_add: 12))
end

p "Tags ✅"


# Read yaml with stories
seed_file = Rails.root.join('lib', 'seeds', 'stories.yml')
config = YAML::load_file(seed_file)
config.each do |story|
  s = Story.new
  I18n.locale = :en
  s.title = story['title_en']
  s.content = story['content_en']
  s.user = author1

  # array_members = []
  # m1 = Member.find(story['author_id'])
  # p m1
  # array_members.push(m1)
  # s.members = array_members

  I18n.locale = :sq
  s.title = story['title_sq']
  s.content = story['content_sq']
  I18n.locale = :sr
  s.title = story['title_sr']
  s.content = story['content_sr']
  s.story_category =  blog

  # Checked
  s.save!
end

p "Stories ✅"

I18n.locale = :en

# Checked for validation errors
governance = MeasurementCategory.create!(
  name_en: "Good governance and access to services",
  name_sr: 'Pristup uslugama i dobro upravljanje',
  name_sq: 'Qeverisja e mirë dhe qasja në shërbime',
  description: Faker::Lorem.paragraph(sentence_count: 2, supplemental: false, random_sentences_to_add: 4),)

justice = MeasurementCategory.create!(
  name_en: "Access to justice",
  name_sr: 'Pristup pravosuđu',
  name_sq: 'Qasja në drejtësi',
  description: Faker::Lorem.paragraph(sentence_count: 2, supplemental: false, random_sentences_to_add: 4),)

inter_religious = MeasurementCategory.create!(
  name_en: "Inter-religious trust-building",
  name_sr: 'Izgradnja međuverskog poverenja',
  name_sq: 'Ndërtimi i mirëbesimit ndër-fetar',
  description: Faker::Lorem.paragraph(sentence_count: 2, supplemental: false, random_sentences_to_add: 4),)

economic = MeasurementCategory.create!(
  name_en: "Economic empowerment and environment",
  name_sr: 'Ekonomsko osnaživanje i životna sredina ',
  name_sq: 'Fuqizimi ekonomik dhe mjedisi',
  description: Faker::Lorem.paragraph(sentence_count: 2, supplemental: false, random_sentences_to_add: 4),)

communication = MeasurementCategory.create!(
  name_en: "Media and communication",
  name_sr: 'Mediji i komunikacija',
  name_sq: 'Media dhe komunikimi',
  description: Faker::Lorem.paragraph(sentence_count: 2, supplemental: false, random_sentences_to_add: 4),)

education = MeasurementCategory.create!(
  name_en: "Education",
  name_sr: 'Obrazovanje',
  name_sq: 'Arsimi',
  description: Faker::Lorem.paragraph(sentence_count: 2, supplemental: false, random_sentences_to_add: 4),)

governance.picture.attach(io: File.open(File.join(Rails.root, "/app/assets/images/seed/kosovo-parliament.png")), filename: "Good governance and access to services image")
justice.picture.attach(io: File.open(File.join(Rails.root, "/app/assets/images/seed/access-to-justices.png")), filename: "Access to justice image")
inter_religious.picture.attach(io: File.open(File.join(Rails.root, "/app/assets/images/seed/inter-religius-trust-building.png")), filename: "Inter-religious trust-building image")
economic.picture.attach(io: File.open(File.join(Rails.root, "/app/assets/images/seed/economic-empowerment.png")), filename: "Economic empowerment and environment image")
communication.picture.attach(io: File.open(File.join(Rails.root, "/app/assets/images/seed/media-communication.png")), filename: "Media and communication image")
education.picture.attach(io: File.open(File.join(Rails.root, "/app/assets/images/seed/education.png")), filename: "Education image")

p "Measurement categories ✅"

# Read csv with recommendations
csv_text = File.read(Rails.root.join('lib', 'seeds', 'recommendations.csv'))
csv = CSV.parse(csv_text, :headers => true, :encoding => 'ISO-8859-1')
csv.each do |row|
  m = Measurement.new
  I18n.locale = :en
  m.measurement_category = eval(row['category'])
  m.title = row['en']
  m.description = row['en_description']
  m.has_fulfillment = row['has_fulfillment']
  m.fulfillment = row['fulfillment']
  I18n.locale = :sq
  m.title = row['sq']
  m.description = row['sq_description']
  I18n.locale = :sr
  m.title = row['sr']
  m.description = row['sr_description']

  # Checked
  m.save!
end

p "Measurements ok ✅"

# Checked
# Create initiatives
completed = InitiativeStatus.create!(
  status_en: "Completed",
  status_sr: "Završeno",
  status_sq: "E përfunduar",
  description: "",
  color: "00d2bc",
)
partially_completed = InitiativeStatus.create!(
  status_en: "Partially completed",
  status_sr: "Delimično završeno",
  status_sq: "E gjyshmëpërfunduar",
  description: "",
  color: "F6CD00"
)
in_progress = InitiativeStatus.create!(
  status_en: "In progress",
  status_sr: "U toku",
  status_sq: "Po vazhdon",
  description: "",
  color: "866AFB"
)
no_activity = InitiativeStatus.create!(
  status_en: "No activity",
  status_sr: "Nema aktivnosti",
  status_sq: "Nuk ka aktivitete",
  description: "",
  color: "FE466C"
)

p "InitiativeStatus ok ✅"

areas_by_seed_id = Hash.new

# Read csv with areas
csv_text = File.read(Rails.root.join('lib', 'seeds', 'location.csv'))
csv = CSV.parse(csv_text, :headers => true, :encoding => 'ISO-8859-1')
csv.each do |row|
  a = Area.new
  a.latitude = row['latitude']
  a.longitude = row['longitude']

  I18n.locale = :en
  a.name = row['location_en']
  I18n.locale = :sq
  a.name = row['location_sq']
  I18n.locale = :sr
  a.name = row['location_sr']

  # Checked
  a.save!

  areas_by_seed_id[row['id']] = a.id
end

p "Areas ok✅"

# Read csv with initiatives
csv_text = File.read(Rails.root.join('lib', 'seeds', 'initiatives.csv'))
csv = CSV.parse(csv_text, :headers => true, :encoding => 'ISO-8859-1')
csv.each do |row|
  i = Initiative.new

  # p 'Titles:'
  # p "Initiative Title: (CSV, en): #{row['initiative_name_en']}"
  # p "Initiative Title: (CSV, sq): #{row['initiative_name_sq']}"
  # p "Initiative Title: (CSV, sr): #{row['initiative_name_sr']}"

  I18n.locale = :en
  i.title = row['initiative_name_en']
  i.description = row['initiative_description_en']
  i.initiative_status = eval(row['status'])
  measurement = Measurement.where(title: row['recommendation']).first

  # p "Measurement (CSV): #{row['recommendation']}"
  # p "Measurement (DB): #{measurement.inspect}"

  i.measurement = measurement
  I18n.locale = :sq
  i.title = row['initiative_name_sq']


  i.description = row['initiative_description_sq']
  I18n.locale = :sr
  i.title = row['initiative_name_sr']
  i.description = row['initiative_description_sr']
  if (!row['start'].nil? && !row['completion'].nil?)
    startTime = DateTime.parse(row['start'])
    endTime = DateTime.parse(row['completion'])
    i.start_date = startTime
    i.end_date = endTime
  end
  if !row['location_ids'].nil?
    areas_array = []
    row['location_ids'].split(',').map(&:strip).each_with_index do |location_id|
      areas_array.push(Area.find(areas_by_seed_id[location_id].to_i))
    end
    i.areas = areas_array
  end
  if !row['actors_en'].nil?
    actors_array = []
    row['actors_en'].split(',').map(&:strip).each_with_index do |actor_name_en, index|
      
      I18n.locale = :en
      actor = Actor.find_by(name: actor_name_en)

      unless actor.nil?
        actors_array.push(actor)

        # p "Found existing actor: #{actor.inspect}"
      else
        actor_name_sr = row['actors_sr'].split(',').map(&:strip)[index]
        actor_name_sq = row['actors_sq'].split(',').map(&:strip)[index]
        
        actor = Actor.create!(
          name_en: actor_name_en,
          name_sr: actor_name_sr,
          name_sq: actor_name_sq,
        )
        actors_array.push(actor)

        # p "Created new actor: #{actor.inspect}"
      end
    end
    i.actors = actors_array
  end
  # Checked
  i.save!
end

p "Initiatives ok ✅"

unless CarouselSlot.count >= 5

  5.times do
    new_slot = CarouselSlot.create(
      link_text_en: "Follow this link",
      link_text_sr: "Slijedite ovaj link",
      link_text_sq: "Ndiqni këtë lidhje",
      carousel_text_en: "Base Carousel", 
      carousel_text_sr: "Osnovni karusel", 
      carousel_text_sq: "Carousel bazë",
      link: "kosovotrustbuilding.com",
      visible: false,
    )

    new_slot.picture.attach(io: File.open(File.join(Rails.root, "/app/assets/images/image.png")), filename: "image")

    new_slot.save!
  end
end

p "Carousel Slots ok ✅"
