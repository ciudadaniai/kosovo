# Rails.application.load_tasks # <---
require 'faker'
require 'csv'

Rake::Task['ktb:permissions'].invoke

#the highest role with all the permissions.
Role.find_or_create_by!(:name => "admin")

#other role
Role.find_or_create_by!(:name => "staff")

#other role
Role.find_or_create_by!(:name => "author")

#assign author the permission to manage all the stories
role = Role.find_by_name('author')
role.permissions << Permission.find_or_create_by!(:subject_class => 'Story', :action => "read", :modifier => "user_id: user.id", :name => "Story:read OWN")
role.permissions << Permission.find_or_create_by!(:subject_class => 'Story', :action => "create", :name => "Story:create")
role.permissions << Permission.find_or_create_by!(:subject_class => 'Story', :action => "edit", :modifier => "user_id: user.id", :name => "Story:edit OWN")
role.permissions << Permission.find_or_create_by!(:subject_class => "rails_admin", :action => "access", :name => "rails_admin:access")
role.permissions << Permission.find_or_create_by!(:subject_class => "dashboard", :action => "read", :name => "dashboard:read")
role.permissions << Permission.find_or_create_by!(:subject_class => "Tag", :action => "read", :name => "Tag:read")
role.permissions << Permission.find_or_create_by!(:subject_class => "Tag", :action => "create", :name => "Tag:create")
role.permissions << Permission.find_or_create_by!(:subject_class => "StoryCategory", :action => "read", :name => "StoryCategory:read")

#other role
Role.create!(:name => "reader")
role = Role.find_by_name('reader')
role.permissions << Permission.find_or_create_by!(:subject_class => 'Story', :action => "index")
role.permissions << Permission.find_or_create_by!(:subject_class => 'Story', :action => "view")
role.permissions << Permission.find_or_create_by!(:subject_class => "rails_admin", :action => "access", :name => "rails_admin:access")
role.permissions << Permission.find_or_create_by!(:subject_class => "dashboard", :action => "read", :name => "dashboard:read")

#assign super admin the permission to manage all the models and controllers
role = Role.find_by_name('admin')
#create a universal permission
role.permissions << Permission.find_or_create_by!(:subject_class => "all", :action => "manage", :name => "all:manage")

# create a user and assign the super admin role to him.
user = User.create(:name => "admin", :email => "ktb@ciudadaniai.org", :password => "adminx", :password_confirmation => "adminx")
user.role = role
user.skip_confirmation!
user.save!

user = User.create(:name => "staff", :email => "staff@ciudadaniai.org", :password => "staffx", :password_confirmation => "staffx", :role_id => Role.find_by_name('staff').id)
user.skip_confirmation!
user.save!
author1 = User.create(:name => "author1", :email => "author1@ciudadaniai.org", :password => "author1", :password_confirmation => "author1", :role_id => Role.find_by_name('author').id)
author1.skip_confirmation!
author1.save!
author2 = User.create(:name => "author2", :email => "author2@ciudadaniai.org", :password => "author2", :password_confirmation => "author2", :role_id => Role.find_by_name('author').id)
author2.skip_confirmation!
author2.save!
user = User.create(:name => "reader", :email => "reader@ciudadaniai.org", :password => "reader", :password_confirmation => "reader", :role_id => Role.find_by_name('reader').id)
user.skip_confirmation!
user.save!
p "Users ✅"

news = StoryCategory.find_or_create_by!(:title => "News", :description => "news category description")
videos = StoryCategory.find_or_create_by!(:title => "Video", :description => "video category description")
blog = StoryCategory.find_or_create_by!(:title => "Blog", :description => "blog category description")
trustbuilding = StoryCategory.find_or_create_by!(:title => "Trustbuilding", :description => "Trustbuilding category description")
champions = StoryCategory.find_or_create_by!(:title => "Champions", :description => "Champions category description")

I18n.locale = :sr
news.update(:title => "News sr", :description => "News category description sr")
videos.update(:title => "Video interviews sr", :description => "Video interviews description sr")

I18n.locale = :sq
news.update(:title => "News sq", :description => "News category description sq")
videos.update(:title => "Video interviews sq", :description => "Video interviews description sq")

p "Story categories ✅"

I18n.locale = :en
Actor.create(name: "Central institutions")
Actor.create(name: "Local institutions")
Actor.create(name: "International organizations")
Actor.create(name: "Civil Society")

I18n.locale = :sr
Actor.update(name: "sr Central institutions")
Actor.update(name: "sr Local institutions")
Actor.update(name: "sr International organizations")
Actor.update(name: "sr Civil Society")

I18n.locale = :sq
Actor.update(name: "sq Central institutions")
Actor.update(name: "sq Local institutions")
Actor.update(name: "sq International organizations")
Actor.update(name: "sq Civil Society")

p "Actors ✅"

I18n.locale = :en

facebook = CategoryLink.create(name: 'facebook')
facebook.icon.attach(io: File.open(File.join(Rails.root, "/app/assets/images/seed/facebook.png")), filename: "facebook")
twitter = CategoryLink.create(name: 'twitter')
twitter.icon.attach(io: File.open(File.join(Rails.root, "/app/assets/images/seed/twitter.png")), filename: "twitter")
instagram = CategoryLink.create(name: 'instagram')
instagram.icon.attach(io: File.open(File.join(Rails.root, "/app/assets/images/seed/instagram.png")), filename: "instagram")
generic = CategoryLink.create(name: 'generic')
generic.icon.attach(io: File.open(File.join(Rails.root, "/app/assets/images/seed/generic.png")), filename: "generic")
linkedin = CategoryLink.create(name: 'linkedin')
linkedin.icon.attach(io: File.open(File.join(Rails.root, "/app/assets/images/seed/linkedin.png")), filename: "linkedin")
youtube = CategoryLink.create(name: 'youtube')
youtube.icon.attach(io: File.open(File.join(Rails.root, "/app/assets/images/seed/youtube.png")), filename: "youtube")
whatsapp = CategoryLink.create(name: 'whatsapp')
whatsapp.icon.attach(io: File.open(File.join(Rails.root, "/app/assets/images/seed/whatsapp.png")), filename: "whatsapp")

p "CategoryLinks ✅"


list_1 = MemberList.find_or_create_by!(:name => "Board")
list_2 = MemberList.find_or_create_by!(:name => "Contributors")
list_3 = MemberList.find_or_create_by!(:name => "Sponsors")

m1 = Member.find_or_create_by!(
  name: Faker::TvShows::GameOfThrones.character,
  bio: Faker::Lorem.sentence(word_count: 3, supplemental: false, random_words_to_add: 4),
  member_list_id: list_1.id,
)
m2 = Member.find_or_create_by!(
  name: Faker::TvShows::GameOfThrones.character,
  bio: Faker::Lorem.sentence(word_count: 3, supplemental: false, random_words_to_add: 4),
  member_list_id: list_1.id,
)
m3 = Member.find_or_create_by!(
  name: Faker::TvShows::GameOfThrones.character,
  bio: Faker::Lorem.sentence(word_count: 3, supplemental: false, random_words_to_add: 4),
  member_list_id: list_1.id,
)
m4 = Member.find_or_create_by!(
  name: Faker::TvShows::GameOfThrones.character,
  bio: Faker::Lorem.sentence(word_count: 3, supplemental: false, random_words_to_add: 4),
  member_list_id: list_2.id,
)
m5 = Member.find_or_create_by!(
  name: Faker::TvShows::GameOfThrones.character,
  bio: Faker::Lorem.sentence(word_count: 3, supplemental: false, random_words_to_add: 4),
  member_list_id: list_2.id,
)
m6 = Member.find_or_create_by!(
  name: Faker::TvShows::GameOfThrones.character,
  bio: Faker::Lorem.sentence(word_count: 3, supplemental: false, random_words_to_add: 4),
  member_list_id: list_3.id,
)
m7 = Member.find_or_create_by!(
  name: Faker::TvShows::GameOfThrones.character,
  bio: Faker::Lorem.sentence(word_count: 3, supplemental: false, random_words_to_add: 4),
  member_list_id: list_3.id,
)

m1.picture.attach(io: File.open(File.join(Rails.root, "/app/assets/images/seed/contributor-01.png")), filename: "Member")
m2.picture.attach(io: File.open(File.join(Rails.root, "/app/assets/images/seed/contributor-01.png")), filename: "Member")
m3.picture.attach(io: File.open(File.join(Rails.root, "/app/assets/images/seed/board-01.png")), filename: "Member")
m4.picture.attach(io: File.open(File.join(Rails.root, "/app/assets/images/seed/contributor-01.png")), filename: "Member")
m5.picture.attach(io: File.open(File.join(Rails.root, "/app/assets/images/seed/board-01.png")), filename: "Member")
m6.picture.attach(io: File.open(File.join(Rails.root, "/app/assets/images/seed/contributor-01.png")), filename: "Member")
m7.picture.attach(io: File.open(File.join(Rails.root, "/app/assets/images/seed/board-01.png")), filename: "Member")

20.times do |link|
  m = MemberLink.create(category_link: CategoryLink.all.sample, link: Faker::Internet.url, member: Member.all.sample)
end

p "Members ✅"

20.times do |tag|
  I18n.locale = :en
  t = Tag.create(name: Faker::Lorem.sentence(word_count: 1, supplemental: false, random_words_to_add: 4))
end

p "Tags ✅"

story1 = Story.find_or_create_by!(
  title: Faker::Lorem.sentence(word_count: 3, supplemental: false, random_words_to_add: 4),
  summary: Faker::Lorem.paragraph(sentence_count: 2, supplemental: false, random_sentences_to_add: 4),
  content: Faker::Markdown.sandwich(sentences: 6, repeat: 3),
  user: author1,
  story_category: StoryCategory.all.sample,
)

story1.update(
  actors: Actor.all.sample(rand(1..3)),
  members: Member.all.sample(rand(1..3)),
  tag_list: Tag.all.sample(rand(1..3)).map{|t| t.name},
)

story2 = Story.find_or_create_by!(
  title: Faker::Lorem.sentence(word_count: 3, supplemental: false, random_words_to_add: 4),
  summary: Faker::Lorem.paragraph(sentence_count: 2, supplemental: false, random_sentences_to_add: 4),
  content: Faker::Markdown.sandwich(sentences: 6, repeat: 3),
  user: author2,
  story_category: StoryCategory.all.sample, )
story2.update(
  actors: Actor.all.sample(rand(1..3)),
  members: Member.all.sample(rand(1..3)),
  tag_list: Tag.all.sample(rand(1..3)).map{|t| t.name},
)

story3 = Story.find_or_create_by!(
  title: Faker::Lorem.sentence(word_count: 3, supplemental: false, random_words_to_add: 4),
  summary: Faker::Lorem.paragraph(sentence_count: 2, supplemental: false, random_sentences_to_add: 4),
  content: Faker::Markdown.sandwich(sentences: 6, repeat: 3),
  user: author1,
  story_category: StoryCategory.all.sample, )
story3.update(
  actors: Actor.all.sample(rand(1..3)),
  members: Member.all.sample(rand(1..3)),
  tag_list: Tag.all.sample(rand(1..3)).map{|t| t.name},
)

story4 = Story.find_or_create_by!(
  title: Faker::Lorem.sentence(word_count: 3, supplemental: false, random_words_to_add: 4),
  summary: Faker::Lorem.paragraph(sentence_count: 2, supplemental: false, random_sentences_to_add: 4),
  content: Faker::Markdown.sandwich(sentences: 6, repeat: 10),
  user: author1,
  story_category: StoryCategory.all.sample, )
story4.update(
  actors: Actor.all.sample(rand(1..3)),
  members: Member.all.sample(rand(1..3)),
  tag_list: Tag.all.sample(rand(1..3)).map{|t| t.name},
)

story5 = Story.find_or_create_by!(
  title: Faker::Lorem.sentence(word_count: 3, supplemental: false, random_words_to_add: 4),
  summary: Faker::Lorem.paragraph(sentence_count: 2, supplemental: false, random_sentences_to_add: 4),
  content: Faker::Markdown.sandwich(sentences: 6, repeat: 9),
  user: author2,
  story_category: StoryCategory.all.sample, )
story5.update(
  actors: Actor.all.sample(rand(1..3)),
  members: Member.all.sample(rand(1..3)),
  tag_list: Tag.all.sample(rand(1..3)).map{|t| t.name},
)

story6 = Story.find_or_create_by!(
  title: Faker::Lorem.sentence(word_count: 3, supplemental: false, random_words_to_add: 4),
  summary: Faker::Lorem.paragraph(sentence_count: 2, supplemental: false, random_sentences_to_add: 4),
  content: Faker::Markdown.sandwich(sentences: 6, repeat: 10),
  user: author2,
  story_category: StoryCategory.all.sample, )
story6.update(
  actors: Actor.all.sample(rand(1..3)),
  members: Member.all.sample(rand(1..3)),
  tag_list: Tag.all.sample(rand(1..3)).map{|t| t.name},
)

story7 = Story.find_or_create_by!(
  title: Faker::Lorem.sentence(word_count: 3, supplemental: false, random_words_to_add: 4),
  summary: Faker::Lorem.paragraph(sentence_count: 2, supplemental: false, random_sentences_to_add: 4),
  content: Faker::Markdown.sandwich(sentences: 6, repeat: 10),
  user: author2,
  story_category: StoryCategory.all.sample, )
story7.update(
  actors: Actor.all.sample(rand(1..3)),
  members: Member.all.sample(rand(1..3)),
  tag_list: Tag.all.sample(rand(1..3)).map{|t| t.name},
)

story8 = Story.find_or_create_by!(
  title: Faker::Lorem.sentence(word_count: 3, supplemental: false, random_words_to_add: 4),
  summary: Faker::Lorem.paragraph(sentence_count: 2, supplemental: false, random_sentences_to_add: 4),
  content: Faker::Markdown.sandwich(sentences: 6, repeat: 11),
  user: author2,
  story_category: StoryCategory.all.sample, )
story8.update(
  actors: Actor.all.sample(rand(1..3)),
  members: Member.all.sample(rand(1..3)),
  tag_list: Tag.all.sample(rand(1..3)).map{|t| t.name},
)

story9 = Story.find_or_create_by!(
  title: Faker::Lorem.sentence(word_count: 3, supplemental: false, random_words_to_add: 4),
  summary: Faker::Lorem.paragraph(sentence_count: 2, supplemental: false, random_sentences_to_add: 4),
  content: Faker::Markdown.sandwich(sentences: 6, repeat: 10),
  user: author2,
  story_category: StoryCategory.all.sample, )
story9.update(
  actors: Actor.all.sample(rand(1..3)),
  members: Member.all.sample(rand(1..3)),
  tag_list: Tag.all.sample(rand(1..3)).map{|t| t.name},
)

story10 = Story.find_or_create_by!(
  title: Faker::Lorem.sentence(word_count: 3, supplemental: false, random_words_to_add: 4),
  summary: Faker::Lorem.paragraph(sentence_count: 2, supplemental: false, random_sentences_to_add: 4),
  content: Faker::Markdown.sandwich(sentences: 6, repeat: 12),
  user: author2,
  story_category: StoryCategory.all.sample, )
story10.update(
  actors: Actor.all.sample(rand(1..3)),
  members: Member.all.sample(rand(1..3)),
  tag_list: Tag.all.sample(rand(1..3)).map{|t| t.name},
)

story11 = Story.find_or_create_by!(
  title: Faker::Lorem.sentence(word_count: 3, supplemental: false, random_words_to_add: 4),
  summary: Faker::Lorem.paragraph(sentence_count: 2, supplemental: false, random_sentences_to_add: 4),
  content: Faker::Markdown.sandwich(sentences: 6, repeat: 3),
  user: author2,
  story_category: StoryCategory.all.sample, )
story11.update(
  actors: Actor.all.sample(rand(1..3)),
  members: Member.all.sample(rand(1..3)),
  tag_list: Tag.all.sample(rand(1..3)).map{|t| t.name},
)

story1.picture.attach(io: File.open(File.join(Rails.root, "/app/assets/images/seed/stories/1.png")), filename: "Story image")
story2.picture.attach(io: File.open(File.join(Rails.root, "/app/assets/images/seed/stories/2.png")), filename: "Story image")
story4.picture.attach(io: File.open(File.join(Rails.root, "/app/assets/images/seed/stories/3.png")), filename: "Story image")
story5.picture.attach(io: File.open(File.join(Rails.root, "/app/assets/images/seed/stories/2.png")), filename: "Story image")
story6.picture.attach(io: File.open(File.join(Rails.root, "/app/assets/images/seed/stories/1.png")), filename: "Story image")
story7.picture.attach(io: File.open(File.join(Rails.root, "/app/assets/images/seed/stories/3.png")), filename: "Story image")
story9.picture.attach(io: File.open(File.join(Rails.root, "/app/assets/images/seed/stories/2.png")), filename: "Story image")
story10.picture.attach(io: File.open(File.join(Rails.root, "/app/assets/images/seed/stories/1.png")), filename: "Story image")


p "Stories ✅"

governance = MeasurementCategory.create(
  name: "Good governance and access to services",
  description: Faker::Lorem.paragraph(sentence_count: 2, supplemental: false, random_sentences_to_add: 4),)
justice = MeasurementCategory.create(
  name: "Access to justice",
  description: Faker::Lorem.paragraph(sentence_count: 2, supplemental: false, random_sentences_to_add: 4),)
inter_religious = MeasurementCategory.create(
  name: "Inter-religious trust-building",
  description: Faker::Lorem.paragraph(sentence_count: 2, supplemental: false, random_sentences_to_add: 4),)
economic = MeasurementCategory.create(
  name: "Economic empowerment and environment",
  description: Faker::Lorem.paragraph(sentence_count: 2, supplemental: false, random_sentences_to_add: 4),)
communication = MeasurementCategory.create(
  name: "Media and communication",
  description: Faker::Lorem.paragraph(sentence_count: 2, supplemental: false, random_sentences_to_add: 4),)
education = MeasurementCategory.create(
  name: "Education",
  description: Faker::Lorem.paragraph(sentence_count: 2, supplemental: false, random_sentences_to_add: 4),)

governance.picture.attach(io: File.open(File.join(Rails.root, "/app/assets/images/seed/kosovo-parliament.png")), filename: "Good governance and access to services image")
justice.picture.attach(io: File.open(File.join(Rails.root, "/app/assets/images/seed/access-to-justices.png")), filename: "Access to justice image")
inter_religious.picture.attach(io: File.open(File.join(Rails.root, "/app/assets/images/seed/inter-religius-trust-building.png")), filename: "Inter-religious trust-building image")
economic.picture.attach(io: File.open(File.join(Rails.root, "/app/assets/images/seed/economic-empowerment.png")), filename: "Economic empowerment and environment image")
communication.picture.attach(io: File.open(File.join(Rails.root, "/app/assets/images/seed/media-communication.png")), filename: "Media and communication image")
education.picture.attach(io: File.open(File.join(Rails.root, "/app/assets/images/seed/education.png")), filename: "Education image")

I18n.locale = :sr
governance.update(name: '1. Pristup uslugama i dobro upravljanje')
justice.update(name: '2. Pristup pravosuđu')
inter_religious.update(name: '3. Izgradnja međuverskog poverenja')
economic.update(name: '4. Ekonomsko osnaživanje i životna sredina ')
communication.update(name: '5. Mediji i komunikacija')
education.update(name: '6. Obrazovanje')

I18n.locale = :sq
governance.update(name: '1. Qeverisja e mirë dhe qasja në shërbime')
justice.update(name: '2. Qasja në drejtësi')
inter_religious.update(name: '3. Ndërtimi i mirëbesimit ndër-fetar')
economic.update(name: '4. Fuqizimi ekonomik dhe mjedisi')
communication.update(name: '5. Media dhe komunikimi')
education.update(name: '6. Arsimi')

p "Measurement categories ✅"

# Read csv with recommendations
csv_text = File.read(Rails.root.join('lib', 'seeds', 'recommendations.csv'))
csv = CSV.parse(csv_text, :headers => true, :encoding => 'ISO-8859-1')
csv.each do |row|
  m = Measurement.new
  I18n.locale = :en
  m.title = row['en']
  m.description = row['en_description']
  m.has_fulfillment = row['has_fulfillment']
  m.fulfillment = row['fulfillment']
  I18n.locale = :sq
  m.title = row['sq']
  I18n.locale = :sr
  m.title = row['sr']
  m.measurement_category = eval(row['category'])
  m.save
end

# 20.times do |index|
#   Measurement.find(index+1).update(has_fulfillment: true, fulfillment: rand(1.0...100.0))
# end
#
# 10.times do |index|
#   Measurement.find(index+1).update(description: Faker::Lorem.paragraph(3))
# end

p "Measurements ✅"

I18n.locale = :en
# Create initiatives
initiative_status1 = InitiativeStatus.create(
  status: "Completed",
  description: Faker::Lorem.paragraph(sentence_count: 2),
  color: "00d2bc",
)
initiative_status2 = InitiativeStatus.create(
  status: "Partially completed",
  description: Faker::Lorem.paragraph(sentence_count: 2),
  color: "F6CD00"
)
initiative_status3 = InitiativeStatus.create(
  status: "In progress",
  description: Faker::Lorem.paragraph(sentence_count: 2),
  color: "866AFB"
)
initiative_status4 = InitiativeStatus.create(
  status: "No activity",
  description: Faker::Lorem.paragraph(sentence_count: 2),
  color: "FE466C"
)

I18n.locale = :sr
initiative_status1.update(status: 'sr Completed')
initiative_status2.update(status: 'sr Partially completed')
initiative_status3.update(status: 'sr In progress')
initiative_status4.update(status: 'sr No activity')

I18n.locale = :sq
initiative_status1.update(status: 'sq Completed')
initiative_status2.update(status: 'sq Partially completed')
initiative_status3.update(status: 'sq In progress')
initiative_status4.update(status: 'sq No activity')

p "InitiativeStatus ✅"

# Create areas

Area.create(name: "Kosovo", latitude: 42.60263, longitude: 20.90297,)
Area.create(name: "Ferizaj/Uroševac", latitude: 42.37018, longitude: 21.14832,)
Area.create(name: "Gjilan/Gnjilane", latitude: 42.46352, longitude: 21.4694,)
Area.create(name: "Hani i Elezit/Elez Han", latitude: 42.1489, longitude: 21.29791,)
Area.create(name: "Kaçanik/Kačanik", latitude: 42.22788, longitude: 21.25685,)
Area.create(name: "Kamenicë / Kamenica", latitude: 42.58757, longitude: 21.57365,)
Area.create(name: "Klokot/Kllokot", latitude: 42.369, longitude: 21.37095,)
Area.create(name: "Novo Brdo/Novobërdë", latitude: 42.60883, longitude: 21.44056,)
Area.create(name: "Parteš/Partesh", latitude: 42.40084, longitude: 21.43323,)
Area.create(name: "Ranilug/Ranillug", latitude: 42.49224, longitude: 21.59851,)
Area.create(name: "Štrpce/Shtërpcë", latitude: 42.24062, longitude: 21.0259,)
Area.create(name: "Viti / Vitina", latitude: 42.32216, longitude: 21.35898,)
Area.create(name: "Gllogoc/Glogovac", latitude: 42.50385, longitude: 21.14713 ,)
Area.create(name: "Leposavić/Leposaviq", latitude: 43.10159, longitude: 20.80111 ,)
Area.create(name: "Mitrovicë/Mitrovica South", latitude: 42.89139, longitude: 20.86599,)
Area.create(name: "Mitrovica/Mitrovicë North", latitude: 42.8947, longitude: 20.86562,)
# Faltan de aquí hacia adelante lat y long
Area.create(name: "Podujevë/Podujevo", latitude: rand(42...42.9), longitude: rand(20...21.5),)
Area.create(name: "Skënderaj/Srbica", latitude: rand(42...42.9), longitude: rand(20...21.5),)
Area.create(name: "Vushtrri/Vučitrn", latitude: rand(42...42.9), longitude: rand(20...21.5),)
Area.create(name: "Zubin Potok", latitude: rand(42...42.9), longitude: rand(20...21.5),)
Area.create(name: "Zvečan/Zveçan", latitude: rand(42...42.9), longitude: rand(20...21.5),)
Area.create(name: "Deçan/Dečani", latitude: rand(42...42.9), longitude: rand(20...21.5),)
Area.create(name: "Gjakovë/Ðakovica", latitude: rand(42...42.9), longitude: rand(20...21.5),)
Area.create(name: "Istog/Istok", latitude: rand(42...42.9), longitude: rand(20...21.5),)
Area.create(name: "Junik", latitude: rand(42...42.9), longitude: rand(20...21.5),)
Area.create(name: "Klinë/Klina", latitude: rand(42...42.9), longitude: rand(20...21.5),)
Area.create(name: "Pejë/Peć", latitude: rand(42...42.9), longitude: rand(20...21.5),)
Area.create(name: "Fushë Kosovë/Kosovo Polje", latitude: rand(42...42.9), longitude: rand(20...21.5),)
Area.create(name: "Gračanica/Graçanicë", latitude: rand(42...42.9), longitude: rand(20...21.5),)
Area.create(name: "Lipjan/Lipljan", latitude: rand(42...42.9), longitude: rand(20...21.5),)
Area.create(name: "Obiliq/Obilić", latitude: rand(42...42.9), longitude: rand(20...21.5),)
Area.create(name: "Prishtinë/Priština", latitude: rand(42...42.9), longitude: rand(20...21.5),)
Area.create(name: "Dragash/Dragaš", latitude: rand(42...42.9), longitude: rand(20...21.5),)
Area.create(name: "Malishevë/Mališevo", latitude: rand(42...42.9), longitude: rand(20...21.5),)
Area.create(name: "Mamuşa/Mamushë/Mamuša", latitude: rand(42...42.9), longitude: rand(20...21.5),)
Area.create(name: "Prizren", latitude: rand(42...42.9), longitude: rand(20...21.5),)
Area.create(name: "Rahovec/Orahovac", latitude: rand(42...42.9), longitude: rand(20...21.5),)
Area.create(name: "Shtime/Štimlje", latitude: rand(42...42.9), longitude: rand(20...21.5),)
Area.create(name: "Suharekë/Suva Reka", latitude: rand(42...42.9), longitude: rand(20...21.5),)

p "Areas ✅"

# Create initiatives
300.times do |index|
  I18n.locale = :en
  i = Initiative.create(
    title: Faker::Lorem.sentence(word_count: 3, supplemental: false, random_words_to_add: 4),
    summary: Faker::Lorem.paragraph(sentence_count: 2, supplemental: false, random_sentences_to_add: 4),
    description: Faker::Lorem.paragraph(sentence_count: 2),
    measurement: Measurement.all.sample,
    initiative_status: InitiativeStatus.all.sample,
    start_date: Faker::Date.backward(days: 50),
    end_date: Faker::Date.forward(days: 200),
    areas: Area.all.sample(rand(1..2)),
    actors: Actor.all.sample(rand(1..3)),
  )
  I18n.locale = :sr
  i.update(title: 'sr initiative' + i.id.to_s)
  I18n.locale = :sq
  i.update(title: 'sq initiative' + i.id.to_s)
end

p "Initiatives ✅"
