class AddHighlightToStories < ActiveRecord::Migration[5.2]
  def change
    add_column :stories, :highlight, :boolean, default: false
  end
end
