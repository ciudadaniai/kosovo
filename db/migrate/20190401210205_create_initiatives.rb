class CreateInitiatives < ActiveRecord::Migration[5.2]
  def change
    create_table :initiatives do |t|
      t.string :title
      t.text :summary
      t.text :description
      t.date :start_date
      t.date :end_date
      t.string :link

      t.timestamps
    end
  end
end
