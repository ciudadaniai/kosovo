class CreateMemberLists < ActiveRecord::Migration[5.2]
  def change
    create_table :member_lists do |t|
      t.string :name

      t.timestamps
    end
  end
end
