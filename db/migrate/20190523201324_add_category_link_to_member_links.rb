class AddCategoryLinkToMemberLinks < ActiveRecord::Migration[5.2]
  def change
    add_reference :member_links, :category_link, foreign_key: true
  end
end
