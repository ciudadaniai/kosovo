class RemoveNonNullFromCs < ActiveRecord::Migration[5.2]
  def change
    change_column_null(:carousel_slots, :link_text, true)
    change_column_null(:carousel_slots, :carousel_text, true)
  end
end
