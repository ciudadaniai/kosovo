class AddColorToInitiativeStatus < ActiveRecord::Migration[5.2]
  def change
    add_column :initiative_statuses, :color, :string
  end
end
