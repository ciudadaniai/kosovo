class AddCarouselSlotInstances < ActiveRecord::Migration[5.2]
  class CarouselSlot < ActiveRecord::Base
  end
  
  def up
    CarouselSlot.reset_column_information
    # This migration was deleted because we shouldn't create
    # Carousel Slots like this.
    # CarouselSlot.create(link_text: "", link: "/", carousel_text: "")
    # CarouselSlot.create(link_text: "", link: "/", carousel_text: "")
    # CarouselSlot.create(link_text: "", link: "/", carousel_text: "")
    # CarouselSlot.create(link_text: "", link: "/", carousel_text: "")
    # CarouselSlot.create(link_text: "", link: "/", carousel_text: "")
  end

  def down
    CarouselSlot.reset_column_information
    CarouselSlot.delete_all
  end
end
