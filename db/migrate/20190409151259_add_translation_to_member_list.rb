class AddTranslationToMemberList < ActiveRecord::Migration[5.2]
  def change
    reversible do |dir|
      dir.up do
        MemberList.create_translation_table! :name => :string
      end

      dir.down do
        MemberList.drop_translation_table!
      end
    end
  end
end
