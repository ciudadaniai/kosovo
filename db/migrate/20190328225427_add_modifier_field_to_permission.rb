class AddModifierFieldToPermission < ActiveRecord::Migration[5.2]
  def change
    add_column :permissions, :modifier, :string, null: true
  end
end
