class AddMemberListIdToMember < ActiveRecord::Migration[5.2]
  def change
    add_reference :members, :member_list, foreign_key: true
  end
end
