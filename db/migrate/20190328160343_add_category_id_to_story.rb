class AddCategoryIdToStory < ActiveRecord::Migration[5.2]
  def change
    
    add_reference :stories, :story_category, foreign_key: true
  end
end
