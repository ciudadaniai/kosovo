class AddSdgIndexToMeasurements < ActiveRecord::Migration[5.2]
  def change
    add_index :measurements, :sdg
  end
end
