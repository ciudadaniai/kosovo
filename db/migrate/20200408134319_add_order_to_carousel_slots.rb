class AddOrderToCarouselSlots < ActiveRecord::Migration[5.2]
  def change
    add_column :carousel_slots, :ordering, :integer
  end
end
