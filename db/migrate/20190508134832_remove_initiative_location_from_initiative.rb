class RemoveInitiativeLocationFromInitiative < ActiveRecord::Migration[5.2]
  def change
    remove_column :initiatives, :initiative_location, :string
  end
end
