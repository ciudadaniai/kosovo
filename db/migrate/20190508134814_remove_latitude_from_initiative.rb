class RemoveLatitudeFromInitiative < ActiveRecord::Migration[5.2]
  def change
    remove_column :initiatives, :latitude, :float
  end
end
