class AddSdgToToMeasurement < ActiveRecord::Migration[5.2]
  def change
    add_column :measurements, :sdg, :string
  end
end
