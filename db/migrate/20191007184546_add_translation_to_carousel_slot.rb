class AddTranslationToCarouselSlot < ActiveRecord::Migration[5.2]
  def up
    CarouselSlot.create_translation_table! :link_text => :string, :carousel_text => :string
  end

  def down
    CarouselSlot.drop_translation_table!
  end
end
