class CreateInitiativeStatuses < ActiveRecord::Migration[5.2]
  def change
    create_table :initiative_statuses do |t|
      t.string :status
      t.text :description
      
      t.timestamps
    end
  end
end
