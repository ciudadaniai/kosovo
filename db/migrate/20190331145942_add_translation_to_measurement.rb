class AddTranslationToMeasurement < ActiveRecord::Migration[5.2]
  def change
    reversible do |dir|
      dir.up do
        Measurement.create_translation_table! :title => :string, :summary => :text, :description => :text
      end

      dir.down do
        Measurement.drop_translation_table!
      end
    end
  end
end
