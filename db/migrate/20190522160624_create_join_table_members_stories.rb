class CreateJoinTableMembersStories < ActiveRecord::Migration[5.2]
  def change
    create_join_table :members, :stories do |t|
      # t.index [:member_id, :story_id]
      # t.index [:story_id, :member_id]
    end
  end
end
