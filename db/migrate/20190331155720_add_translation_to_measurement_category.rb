class AddTranslationToMeasurementCategory < ActiveRecord::Migration[5.2]
  def change
    reversible do |dir|
      dir.up do
        MeasurementCategory.create_translation_table! :name => :string, :description => :text
      end

      dir.down do
        MeasurementCategory.drop_translation_table!
      end
    end
  end
end
