class CreateMeasurements < ActiveRecord::Migration[5.2]
  def change
    create_table :measurements do |t|
      t.string :title
      t.text :summary
      t.text :description

      t.timestamps
    end
  end
end
