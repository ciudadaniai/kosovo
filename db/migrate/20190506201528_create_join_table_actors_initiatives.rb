class CreateJoinTableActorsInitiatives < ActiveRecord::Migration[5.2]
  def change
    create_join_table :actors, :initiatives do |t|
      # t.index [:actor_id, :initiative_id]
      # t.index [:initiative_id, :actor_id]
    end
  end
end
