class AddTranslationToActor < ActiveRecord::Migration[5.2]
  def change
    reversible do |dir|
      dir.up do
        Actor.create_translation_table! :name => :string
      end

      dir.down do
        Actor.drop_translation_table!
      end
    end
  end
end
