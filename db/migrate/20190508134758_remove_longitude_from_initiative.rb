class RemoveLongitudeFromInitiative < ActiveRecord::Migration[5.2]
  def change
    remove_column :initiatives, :longitude, :float
  end
end
