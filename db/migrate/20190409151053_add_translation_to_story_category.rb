class AddTranslationToStoryCategory < ActiveRecord::Migration[5.2]
  def change
    reversible do |dir|
      dir.up do
        StoryCategory.create_translation_table! :title => :string, :description => :string
      end

      dir.down do
        StoryCategory.drop_translation_table!
      end
    end
  end
end
