class CreateCarouselSlots < ActiveRecord::Migration[5.2]
  def up
    create_table :carousel_slots do |t| 
    # A Carousel Slot will have:
    # - A link, composed of two pieces:
    #  The displayed text (globalized!)
        t.string :link_text, :limit => 20, :null => false
    #  The link itself
        t.string :link, :null => false
    # - Text for the Carousel Slot (globalized!)
        t.string :carousel_text, :limit => 100, :null => false
    # - A toggle field (boolean) to make the slot visible in the page or not.
        t.boolean :visible, :default => false, :null => false

    # - And an image (handled internally by Rails)
    end

    # Reminders:
    # - Images are not stored in the database
    # - Globalization of fields is done in a separate migration which generates
    #   a custom table for that purpose.

  end

  def down
    drop_table :carousel_slots
  end
end
