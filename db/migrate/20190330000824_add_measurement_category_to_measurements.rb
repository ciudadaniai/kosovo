class AddMeasurementCategoryToMeasurements < ActiveRecord::Migration[5.2]
  def change
    add_reference :measurements, :measurement_category, foreign_key: true
  end
end
