class AddMeasurementToInitiatives < ActiveRecord::Migration[5.2]
  def change
    add_reference :initiatives, :measurement, foreign_key: true
  end
end
