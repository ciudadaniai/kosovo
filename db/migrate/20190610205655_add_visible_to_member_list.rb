class AddVisibleToMemberList < ActiveRecord::Migration[5.2]
  def change
    add_column :member_lists, :visible, :boolean
  end
end
