class CreateEvents < ActiveRecord::Migration[5.2]
  def change
    create_table :events do |t|
      t.string :name
      t.timestamp :startdate
      t.timestamp :enddate
      t.timestamp :starttime
      t.timestamp :endtime
      t.string :color
      t.string :url

      t.timestamps null: false
    end
  end
end
