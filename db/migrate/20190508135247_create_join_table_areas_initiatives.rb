class CreateJoinTableAreasInitiatives < ActiveRecord::Migration[5.2]
  def change
    create_join_table :areas, :initiatives do |t|
      # t.index [:area_id, :initiative_id]
      # t.index [:initiative_id, :area_id]
    end
  end
end
