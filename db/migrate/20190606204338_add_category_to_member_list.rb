class AddCategoryToMemberList < ActiveRecord::Migration[5.2]
  def change
    add_column :member_lists, :category, :string
  end
end
