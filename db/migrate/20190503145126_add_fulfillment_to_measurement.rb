class AddFulfillmentToMeasurement < ActiveRecord::Migration[5.2]
  def change
    add_column :measurements, :has_fulfillment, :boolean
    add_column :measurements, :fulfillment, :float
  end
end
