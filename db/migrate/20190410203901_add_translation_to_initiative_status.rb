class AddTranslationToInitiativeStatus < ActiveRecord::Migration[5.2]
  def change
    reversible do |dir|
      dir.up do
        InitiativeStatus.create_translation_table! :status => :string, :description => :text
      end

      dir.down do
        InitiativeStatus.drop_translation_table!
      end
    end
  end
end
