class AddStatusRefToInitiative < ActiveRecord::Migration[5.2]
  def change
    add_reference :initiatives, :initiative_status, foreign_key: true
  end
end
