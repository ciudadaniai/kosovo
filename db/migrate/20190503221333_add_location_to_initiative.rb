class AddLocationToInitiative < ActiveRecord::Migration[5.2]
  def change
    add_column :initiatives, :longitude, :float
    add_column :initiatives, :latitude, :float
    add_column :initiatives, :initiative_location, :string
  end
end
