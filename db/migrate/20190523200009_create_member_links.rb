class CreateMemberLinks < ActiveRecord::Migration[5.2]
  def change
    create_table :member_links do |t|
      t.string :link

      t.timestamps
    end
  end
end
