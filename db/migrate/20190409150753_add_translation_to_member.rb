class AddTranslationToMember < ActiveRecord::Migration[5.2]
  def change
    reversible do |dir|
      dir.up do
        Member.create_translation_table! :bio => :text
      end

      dir.down do
        Member.drop_translation_table!
      end
    end
  end
end
