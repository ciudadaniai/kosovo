class CreateJoinTableActorsStories < ActiveRecord::Migration[5.2]
  def change
    create_join_table :actors, :stories do |t|
      # t.index [:actor_id, :story_id]
      # t.index [:story_id, :actor_id]
    end
  end
end
