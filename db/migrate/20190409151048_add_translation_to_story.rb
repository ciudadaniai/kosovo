class AddTranslationToStory < ActiveRecord::Migration[5.2]
  def change
    reversible do |dir|
      dir.up do
        Story.create_translation_table! :title => :string, :summary => :text, :content => :text
      end

      dir.down do
        Story.drop_translation_table!
      end
    end
  end
end
