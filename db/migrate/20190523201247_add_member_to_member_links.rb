class AddMemberToMemberLinks < ActiveRecord::Migration[5.2]
  def change
    add_reference :member_links, :member, foreign_key: true
  end
end
