## Getting Started

### Pre-Requisites

We need to install these libraries before we're able to run the project.

#### Linux

- **RVM**, Ruby Version Manager: https://rvm.io/rvm/install
- **Bundler**:
```bash
gem install bundle
```
- **PostgreSQL**:
```bash
sudo apt-get install postgresql-10 postgresql-server-dev-10
```
- **NodeJS**:
```bash
sudo apt-get install nodejs
```

#### Mac OS

You need to have installed [xcode](https://developer.apple.com/xcode/) and [brew](https://brew.sh).

- **RVM**, Ruby Version Manager: https://rvm.io/rvm/install
- **Bundler**:
```bash
gem install bundle
```
- **PostgreSQL**:
```bash
brew install postgres
```

Start PostgreSQL server
```bash
pg_ctl -D /usr/local/var/postgres start
```

- **NodeJS**:
```bash
brew install node
```

### Setting up the Database

In Terminal, type: `sudo -u postgres psql`. You will now be in the PostgreSQL command line.

There, you create the user and database for development, like this:

```sql
CREATE ROLE kosovo LOGIN password 'k0s0v0';
ALTER USER kosovo CREATEDB;

CREATE DATABASE kosovo_development ENCODING 'UTF8' OWNER kosovo;
```

Now you can exit the `psql` command line with `\q`.

### Setting up the Repo

```bash
git clone https://gitlab.com/ciudadaniai/kosovo
cp config/database.yml.example config/database.yml
```

### Set up the Server

```bash
bundle install
rake db:setup
rake pg_search:multisearch:rebuild\[Initiative\]
rake pg_search:multisearch:rebuild\[Story\]
rake pg_search:multisearch:rebuild\[Measurement\]
rails s
```

#### Host a Server instance locally to test with other devices

Sometimes we'd like to run the server locally but test it on another device, e.g. a Windows pc or a mobile phone. For those needs, we can use this command:

```bash
rails server -b 0.0.0.0
```

To enter the site, we type this in the address bar:

```bash
<IP>:3000
```

Where `<IP>` is the local IP of your computer (usually it looks like `192.168.X.Y`)

### Gemfile

* https://github.com/twbs/bootstrap-rubygem#a-ruby-on-rails

### Important Links

https://stackoverflow.com/questions/16736891/pgerror-error-new-encoding-utf8-is-incompatible

https://blog.joshsoftware.com/2012/10/23/dynamic-roles-and-permissions-using-cancan/

https://phraseapp.com/blog/posts/rails-i18n-setting-and-managing-locales/

https://stackoverflow.com/questions/34959453/how-to-load-different-assets-based-on-my-layout

https://www.linode.com/docs/development/ror/ruby-on-rails-nginx-debian/
